/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie können es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 *     veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es nützlich sein wird, aber
 *     OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License für weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem;

import java.awt.*;

/**
 * Sammelklasse fuer alle Konstanten und Einstellungen.
 *
 * @author Dennis van der Wals
 */
public class CommonSettings {
    /* Legt fest, welche Funktionen in welcher Weise angeboten werden. Standard = true */
    public static final boolean DEVELOPER = true;

    /* Legt fest, wie gross der maximale Anteil des Radius im Verhaeltnis zum minimalem Anteil sein soll. Standard =
    40 */
    public static final int MAX_ANTEIL_RADIUS = 40;

    /* Legt die Standardeinstellungen fuer den Anteil des Radius an der Gesamtbreite fest. Standards = 5 / 200 / 25 */
    public static final int MIN_ANTEIL_RADIUS_START = 5, MAX_ANTEIL_RADIUS_START = 200, ANTEIL_RADIUS_START = 25;
    /* Standards = 0 / 180 */
    public static final int DEGREE_0 = 0, DEGREE_180 = 180;
    /* Legt die Einstellungen für die Kantendicke fest. Standards = 2 / 5 */
    public static final int BASIC_STROKE = 2, HIGHLIGHTED_STROKE = 5;
    /* Legt fest, wie Nahe man einer Kante mindestens sein muss, um sie zu markieren. Standard = 0.2 */
    public static final double MINIMUM_DISTANCE_TO_EDGE = 0.2;
    /* Legt die groesser der Fenster bei der Generierung fest. Standards = 800 / 600 */
    private static final int WIDTH = 800, HEIGHT = 600;
    public static final Dimension WINDOW_SIZE_START = new Dimension(WIDTH, HEIGHT);
}
