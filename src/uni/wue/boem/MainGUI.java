/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie können es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 *     veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es nützlich sein wird, aber
 *     OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License für weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem;

import uni.wue.boem.aktionen.Aktion;
import uni.wue.boem.aktionen.AktionManager;
import uni.wue.boem.aktionen.BearbeiteSeitenAktion;
import uni.wue.boem.daten.Graph;
import uni.wue.boem.daten.Loeser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Hauptklasse fuer die Buchbindung. Stellt Startmethode und Konstrukt der
 * graphischen Oberflaeche sowie Bedienelemente bereit.
 *
 * @author Dennis van der Wals
 */
final class MainGUI {
    /**
     * Toggles Developer Mode! Set 'false' for contest!
     */
    /* GUI Elemente */
    private JButton rueckgaengig, wiederherstellen, hinzufuegen, loesenPermutation, loeseKanten;
    private JButton speichern, oeffnen;
    private JMenuItem speichernM, oeffnenM, neuerGraph;
    private ActionListener buttonActions;
    private JSlider anteilRadius;
    private JFrame frame;
    private WindowListener windowClosing;
    private JLabel label;
    /* Daten Elemente */
    private Graph graph;
    private AktionManager aktionManager;
    private Loeser loeser;
    private boolean guiBlockiert;
    private GraphGUIManager guiManager;

    /**
     * Initialisiert die Oberflaeche und die Aktionen.
     */
    private MainGUI() {
        erzeugeOberflaeche();
        aktionenManagerZuruecksetzen();

        getFrame().setVisible(true);
    }

    /**
     * Startmethode
     *
     * @param args keine Funktion
     */
    public static void main(String[] args) {
        //noinspection EmptyCatchBlock
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {

        }
        new MainGUI();
    }

    /**
     * Hier wird die Oberflaeche zusammengesetzt und Designed.
     */
    private void erzeugeOberflaeche() {
        JMenuBar menuBar = new JMenuBar();

        if (CommonSettings.DEVELOPER) {
            JMenu datei = new JMenu("File");
            datei.add(getNeuerGraph());
            datei.add(getOeffnenMenu());
            datei.add(getSpeichernMenu());
            menuBar.add(datei);
        } else {
            menuBar.add(getOeffnen());
            menuBar.add(getSpeichern());
        }

        menuBar.add(getRueckgaengig());
        menuBar.add(getWiederherstellen());
        menuBar.add(getHinzufuegen());
        if (CommonSettings.DEVELOPER) {
            menuBar.add(getLoesenPermutation());
            menuBar.add(getLoeseKanten());
        }

        JPanel buttons = new JPanel(new GridLayout(1, 4, 5, 5));
        buttons.add(getLabelKreuzungen());

        JPanel top = new JPanel(new BorderLayout(5, 5));
        top.add(buttons, BorderLayout.WEST);
        top.add(getAnteilRadius(), BorderLayout.EAST);

        getFrame().setJMenuBar(menuBar);
        getFrame().setLayout(new BorderLayout(5, 5));
        getFrame().add(top, BorderLayout.NORTH);

        GraphGUI defGraphGui = getGuiManager().addGraphGui(aktionManager);
        defGraphGui.getFrame().setVisible(false);

        getFrame().add(defGraphGui.getBuch(), BorderLayout.CENTER);
    }

    /**
     * @return the speichern
     */
    private JButton getSpeichern() {
        if (speichern == null) {
            speichern = new JButton(new ImageIcon(MainGUI.class.getResource("images/Save16.gif")));
            speichern.setEnabled(false);
            speichern.setToolTipText("Save the actual Graph");
            speichern.addActionListener(getButtonActions());
        }
        return speichern;
    }

    /**
     * @return the speichern
     */
    private JMenuItem getSpeichernMenu() {
        if (speichernM == null) {
            speichernM = new JMenuItem("Save");
            speichernM.setIcon(new ImageIcon(MainGUI.class.getResource("images/Save16.gif")));
            speichernM.setEnabled(false);
            speichernM.setToolTipText("Save the actual Graph");
            speichernM.addActionListener(getButtonActions());
        }
        return speichernM;
    }


    private JLabel getLabelKreuzungen() {
        if (label == null) {
            label = new JLabel(" crossings: ");
            label.setToolTipText("# of crossings in graph");
        }
        return label;
    }


    /**
     * @return the oeffnen
     */
    private JButton getOeffnen() {
        if (oeffnen == null) {
            oeffnen = new JButton(new ImageIcon(MainGUI.class.getResource("images/Open16.gif")));
            oeffnen.setEnabled(true);
            oeffnen.setToolTipText("Opens a Graph from a File");
            oeffnen.addActionListener(getButtonActions());
        }
        return oeffnen;
    }

    /**
     * @return the oeffnen
     */
    private JMenuItem getOeffnenMenu() {
        if (oeffnenM == null) {
            oeffnenM = new JMenuItem("Open");
            oeffnenM.setIcon(new ImageIcon(MainGUI.class.getResource("images/Open16.gif")));
            oeffnenM.setEnabled(true);
            oeffnenM.setToolTipText("Opens a Graph from a File");
            oeffnenM.addActionListener(getButtonActions());
        }
        return oeffnenM;
    }

    /**
     * @return the rueckgaengig
     */
    private JButton getRueckgaengig() {
        if (rueckgaengig == null) {
            rueckgaengig = new JButton(new ImageIcon(MainGUI.class.getResource("images/Undo16.gif")));
            rueckgaengig.setEnabled(false);
            rueckgaengig.setToolTipText("Undo");
            rueckgaengig.addActionListener(getButtonActions());
        }
        return rueckgaengig;
    }

    /**
     * @return the wiederherstellen
     */
    private JButton getWiederherstellen() {
        if (wiederherstellen == null) {
            wiederherstellen = new JButton(new ImageIcon(MainGUI.class.getResource("images/Redo16.gif")));
            wiederherstellen.setEnabled(false);
            wiederherstellen.setToolTipText("Redo");
            wiederherstellen.addActionListener(getButtonActions());
        }
        return wiederherstellen;
    }

    /**
     * @return the buttonActions
     */
    private ActionListener getButtonActions() {
        if (buttonActions == null) {
            buttonActions = e -> {
                if (guiBlockiert)
                    return;

                Aktion a = null;

                if (e.getSource() == getSpeichern() || e.getSource() == getSpeichernMenu()) {
                    if (graph.speichern() == 1)
                        /*
                         * Nachdem eine Datei gespeichert wurde, kann keine
                         * Aktion rueckgaengig oder wiederholt werden.
                         */
                        aktionenManagerZuruecksetzen();

                } else if (e.getSource() == getOeffnen() || e.getSource() == getOeffnenMenu()) {
                    if (graphWechselVorbereiten()) {
                        if (IO.laden()) {
                            setGraph(new Graph(IO.getKnoten(), IO.getSeiten()));

                        } else {
                            return;
                        }
                    }

                } else if (e.getSource() == getRueckgaengig()) {
                    a = aktionManager.rueckgaengig();
                    guiManager.aktualisiereDarstellung();

                } else if (e.getSource() == getWiederherstellen()) {
                    a = aktionManager.wiederherstellen();
                    guiManager.aktualisiereDarstellung();

                } else if (e.getSource() == getNeuerGraph()) {
                    if (graphWechselVorbereiten())
                        setGraph(new Graph());

                } else if (e.getSource() == getHinzufuegen())
                    guiManager.addGraphGui(aktionManager);

                else if (e.getSource() == getLoesenPermutation() && loeser != null)
                    new Thread(loeser::loeseMitHeuristik).start();

                else if (e.getSource() == getLoeseKanten() && loeser != null)
                    new Thread(loeser::minimiereSeiten).start();

                if (a instanceof BearbeiteSeitenAktion)
                    guiManager.aktualisiereLeisten();

                SwingUtilities.invokeLater(() -> {
                    getFrame().revalidate();
                    getFrame().repaint();
                });
            };
        }
        return buttonActions;
    }

    /**
     * Prueft auf Aenderungen des aktuellen Graphen und bietet die Speicherung an.
     *
     * @return Gibt zurueck, ob der aktuelle graph ersetzt werden kann.
     */
    private boolean graphWechselVorbereiten() {
        switch (pruefeAufAenderungen()) {
            case JOptionPane.CANCEL_OPTION:
                // Bricht den Graphwechsel ab.
                return false;

            case JOptionPane.NO_OPTION:
                // Ersetzt den Graphen ohne Ruecksicht auf bisherige Graphen.
                return true;

            default:
                break;
        }

		/* Versucht die Aenderungen zu speichern */
        boolean error = graph.speichern() == -1;

		/*
         * Bei einem Fehler beim Speichern wird der Benutzer gefragt, ob die
		 * neue Datei dennoch geladen werden soll.
		 */
        int auswahl = JOptionPane.NO_OPTION;
        if (error)
            auswahl = JOptionPane.showConfirmDialog(null, "Changes could not be saved.\n Load new file anyway?", "Don't save changes.", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);

		/*
         * Liegt kein Fehler vor oder ist der Benutzer bereit, die Daten zu
		 * verlieren, wird die neue Datei geoeffnet.
		 */
        return (auswahl == JOptionPane.YES_OPTION || !error);
    }

    /**
     * Prueft anhand des Rueggaengig-Knopfes, ob Aenderungen durchgefuehrt
     * wurden, die noch nicht gespeichert sind. Oeffnet dann einen Dialog um zu
     * fragen, ob diese Aenderungen gespeichert werden sollen. Gibt es keine
     * Aenderungen oder sollen diese verworfen werden, wird
     * <code>JOptionPane.NO_OPTION</code> zurueck gegeben.
     *
     * @return <code>JOptionPane.NO_OPTION</code> wenn Aenderungen verworfen werden sollen,
     * <code>JOptionPane.CANCEL_OPTION</code>, wenn abgebrochen werden soll und
     * <code>JOptionPane.YES_OPTION</code>, wenn die Aenderungen gespeichert werden
     * sollen.
     */
    private int pruefeAufAenderungen() {
        int auswahl = JOptionPane.NO_OPTION;
        if (getRueckgaengig().isEnabled()) {
            auswahl = JOptionPane.showConfirmDialog(null, "Shall the changes be saved?", "Save changes.", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);

        }
        return auswahl;
    }

    /**
     * @param graph the graph to set
     */
    private void setGraph(Graph graph) {
        this.graph = graph;

		/*
         * Setze das Minimum des Radius so, dass immer alle Knoten angezeigt
		 * werden koennen. Andernfalls koennte der Anteil an der Darstellung
		 * groesser werden, als die Darstellung selbst ist
		 */
        getAnteilRadius().setMinimum(1);

        if (graph.anzahlKnoten() > 0)
            getAnteilRadius().setMinimum(graph.anzahlKnoten());
        getAnteilRadius().setMaximum(getAnteilRadius().getMinimum() * CommonSettings.MAX_ANTEIL_RADIUS);

        getSpeichern().setEnabled(true);
        getSpeichernMenu().setEnabled(true);
        getLoesenPermutation().setEnabled(true);
        getLoeseKanten().setEnabled(true);
        aktionenManagerZuruecksetzen();

        graph.setLabelKreuzungen(getLabelKreuzungen());

        guiManager.setGraph(graph);
        guiManager.aktualisiereDarstellung();
        loeser = new Loeser(graph, guiManager);

        SwingUtilities.invokeLater(() -> {
            getFrame().revalidate();
            getFrame().repaint();
        });
    }

    /**
     * @return the anteilRadius
     */
    private JSlider getAnteilRadius() {
        if (anteilRadius == null) {
            anteilRadius = new JSlider(JSlider.HORIZONTAL, CommonSettings.MIN_ANTEIL_RADIUS_START, CommonSettings.MAX_ANTEIL_RADIUS_START, CommonSettings.ANTEIL_RADIUS_START);
            anteilRadius.setToolTipText("Determines the size of the vertices as fraction of the shown area.");
            anteilRadius.addChangeListener(e -> guiManager.setAnteilRadius(anteilRadius.getValue()));
        }
        return anteilRadius;
    }

    /**
     * Ersetzt den alten AktionenManager gegen einen neuen, da dessen Aktionen
     * nicht mehr gueltig sind.
     */
    private void aktionenManagerZuruecksetzen() {
        this.aktionManager = new AktionManager(getRueckgaengig(), getWiederherstellen());
        guiManager.setAktionManager(aktionManager);
    }

    /**
     * @return the frame
     */
    private JFrame getFrame() {
        if (frame == null) {
            frame = new JFrame("GD-Book-Embedding");
            frame.setSize(CommonSettings.WINDOW_SIZE_START);
            frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            frame.addWindowListener(getWindowClosing());
        }
        return frame;
    }

    /**
     * @return the windowClosing
     */
    private WindowListener getWindowClosing() {
        if (windowClosing == null)
            windowClosing = new WindowListener() {

                @Override
                public void windowOpened(WindowEvent e) {
                }

                @Override
                public void windowIconified(WindowEvent e) {
                }

                @Override
                public void windowDeiconified(WindowEvent e) {
                }

                @Override
                public void windowDeactivated(WindowEvent e) {
                }

                @Override
                public void windowClosing(WindowEvent e) {
                    switch (pruefeAufAenderungen()) {
                        case JOptionPane.YES_OPTION:
                            graph.speichern();

                        case JOptionPane.NO_OPTION:
                            System.exit(0);
                            break;

                        default:
                            break;
                    }

                }

                @Override
                public void windowClosed(WindowEvent e) {
                }

                @Override
                public void windowActivated(WindowEvent e) {
                }
            };

        return windowClosing;
    }

    /**
     * @return the neuerGraph
     */
    private JMenuItem getNeuerGraph() {
        if (neuerGraph == null) {
            neuerGraph = new JMenuItem("New");
            neuerGraph.setIcon(new ImageIcon(MainGUI.class.getResource("images/New16.gif")));
            neuerGraph.setToolTipText("Create new graph");
            neuerGraph.addActionListener(getButtonActions());
        }
        return neuerGraph;
    }

    /**
     * @return hinzufuegen Button
     */
    private JButton getHinzufuegen() {
        if (hinzufuegen == null) {
            hinzufuegen = new JButton(" + ");
            hinzufuegen.setToolTipText("Enhance View");
            hinzufuegen.addActionListener(getButtonActions());
        }
        return hinzufuegen;
    }

    /**
     * @return loesen Button
     */
    private JButton getLoesenPermutation() {
        if (loesenPermutation == null) {
            loesenPermutation = new JButton("Permute");
            loesenPermutation.setToolTipText("Removes crossings with heuristic by rearranging nodes and edges");
            loesenPermutation.addActionListener(getButtonActions());
            loesenPermutation.setEnabled(false);
        }
        return loesenPermutation;
    }

    /**
     * @return loesen Button
     */
    private JButton getLoeseKanten() {
        if (loeseKanten == null) {
            loeseKanten = new JButton("Reassign");
            loeseKanten.setToolTipText("Removes crossings with heuristic by rearranging edges");
            loeseKanten.addActionListener(getButtonActions());
            loeseKanten.setEnabled(false);
        }
        return loeseKanten;
    }

    /**
     * @return gui Manager
     */
    private GraphGUIManager getGuiManager() {
        if (guiManager == null)
            guiManager = new GraphGUIManager(this);

        return guiManager;
    }

    /**
     * Blockiert die GUI oder gibt sie frei.
     *
     * @param guiBlockiert Legt fest, ob die GUI blockiert werden soll.
     */
    void setGuiBlockiert(boolean guiBlockiert) {
        this.guiBlockiert = guiBlockiert;
    }

}
