/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie k�nnen es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder sp�teren
 *     ver�ffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es n�tzlich sein wird, aber
 *     OHNE JEDE GEW�HRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gew�hrleistung der MARKTF�HIGKEIT oder EIGNUNG F�R EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License f�r weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem.daten;

import uni.wue.boem.elemente.Kreis;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;
import java.util.stream.Collectors;

/**
 * Klasse repraesentiert einen Knoten des Graphen und enthaelt information darueber, wie der Knoten gezeichnet wird. Die Klasse implementiert das Comparable-Interface und l�sst sich soriteren, wobei die Reihenfolge der Mittelpunkte der Kreise, die den Knoten repraesentieren, bentutzt wird.
 *
 * @author Dennis van der Wals
 */
public class Knoten implements Comparable<Knoten> {
    private int nummer;
    private boolean gesetzt;
    private LinkedList<Kante> kanten;
    private Kreis kreis;

    /**
     * Konstruktor.
     *
     * @param nummer Nummer des Knoten. Muss groe�er 0 sein.
     */
    public Knoten(int nummer) {
        if (nummer < 0)
            throw new IllegalArgumentException("The label number of a vertex must be non-negative.");

        this.nummer = nummer;
        this.kanten = new LinkedList<>();
        this.kreis = new Kreis(this);
    }

    /**
     * @return the nummer
     */
    public int getNummer() {
        return nummer;
    }

    /**
     * @return the kanten
     */
    public Queue<Kante> getKanten() {
        return kanten;
    }

    void entferneKante(Kante k) {
        if (k != null)
            kanten.remove(k);
    }

    public String toString() {
        return nummer + "";
    }

    public boolean equals(Object o) {
        return o instanceof Knoten && ((Knoten) o).nummer == this.nummer;
    }

    /**
     * @return the kreis
     */
    public Kreis getKreis() {
        return kreis;
    }

    @Override
    public int compareTo(Knoten o) {
        if (o == null)
            return 1;

        if (o.getKreis().getMittelpunkt().x > this.getKreis().getMittelpunkt().x)
            return -1;
        else if (o.getKreis().getMittelpunkt().x < this.getKreis().getMittelpunkt().x)
            return 1;
        return 0;
    }

    /**
     * Loescht alle Kanten des Knotens, wenn der Knoten geloescht wird.
     *
     * @return Eine Liste der Kanten des Knotens, bevor sie geloescht werden.
     */
    Collection<Kante> loeschen() {
        Collection<Kante> kanten = new LinkedList<>();
        kanten.addAll(this.kanten);
        this.kanten.forEach(Kante::loescheKante);

        return kanten;
    }

    /**
     * Gibt zurueck, ob der Knoten schon gesetzt wurde.
     *
     * @return Wahrheitswert.
     */
    public boolean isGesetzt() {
        return gesetzt;
    }

    /**
     * Legt fest, ob der Knoten schon gesetzt wurde.
     *
     * @param gesetzt Wahrheitswert
     */
    public void setGesetzt(boolean gesetzt) {
        this.gesetzt = gesetzt;
    }

    /**
     * Zaehlt, wie viele Nachbarknoten gesetzt wurden und wie viele nicht. Gibt die Anzahl in einem Array zurueck.
     *
     * @return [Anzahl gesetzter Nachbarn, Anzahl nicht gesetzter Nachbarn]
     */
    private int[] nachbarn() {
        int[] nachbarn = new int[2];

        for (Kante kante : this.kanten)
            if (kante.getNachbarn(this).isGesetzt())
                nachbarn[0]++;
            else
                nachbarn[1]++;

        return nachbarn;
    }

    /**
     * Gibt die Anzahl der Nachbarn zurueck, die nicht gesetzt sind und selber Nachbarn besitzen, die gesetzt sind.
     * Dadurch wird die Anzahl der Knoten ermittelt, die noch offene Verbindungen besitzen.
     *
     * @return Anzahl an nicht gesetzten Nachbarn, die gesetzte Nachbarn besitzen.
     */
    public int nichtGesetzteNachbarnMitOffenenKanten() {
        int nachbarnMitOffenenKanten = 0;

        /* Die Mindestanzahl Nachbarn stellt sicher, dass der Knoten selbst nicht als Nachbar der Nachbarn gezaehlt
        wird */
        int mindestAnzahlNachbarn = 0;
        if (gesetzt)
            mindestAnzahlNachbarn = 1;

        for (Kante k : kanten) {
            Knoten nachbar = k.getNachbarn(this);
            if (!nachbar.isGesetzt() && nachbar.nachbarn()[0] > mindestAnzahlNachbarn) {
                nachbarnMitOffenenKanten++;
            }
        }

        return nachbarnMitOffenenKanten;
    }

    /**
     * Gibt eine Liste mit Kanten des Knotens zurueck, die andere Kanten schneiden.
     *
     * @return Liste mit schneidenden Kanten.
     */
    LinkedList<Kante> kreuzendeKanten() {
        return kanten.stream().filter(kante -> kante.zaehleKreuzendeKanten() > 0).collect(Collectors.toCollection(LinkedList::new));
    }
}
