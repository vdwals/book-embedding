/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie können es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 *     veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es nützlich sein wird, aber
 *     OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License für weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem.daten;

import uni.wue.boem.IO;

import javax.swing.*;
import java.awt.*;
import java.util.*;

/**
 * Diese Klasse repraesentiert die Datenstruktur des Graphen sowie alle Operationen auf diesen und dessen Elementen. Die Kanten des Graphen sind dabei lediglich durch die Knoten und Seiten referenziert.
 *
 * @author Dennis van der Wals
 */
public class Graph {
    private final LinkedList<Knoten> knoten;
    private Seite[] seiten;

    private JLabel labelKreuzungen;

    /**
     * Erzeugt einen neuen, leeren Graphen
     */
    public Graph() {
        this.knoten = new LinkedList<>();
        this.seiten = new Seite[1];
        this.seiten[0] = new Seite(0, Color.black);
    }

    /**
     * Erzeugt einen neuen Graphen aus den uebergebenen Daten.
     *
     * @param knoten Liste mit Knoten. Wenn <code>null</code>, wird eine leere Liste erzeugt.
     * @param seiten Array mit Seiten. Wenn <code>null</code>, wird eine neue Seite erzeugt.
     */
    public Graph(LinkedList<Knoten> knoten, Seite[] seiten) {
        if (knoten != null)
            this.knoten = knoten;
        else
            this.knoten = new LinkedList<>();

        if (seiten != null && seiten.length > 0)
            this.seiten = seiten;
        else {
            this.seiten = new Seite[1];
            this.seiten[0] = new Seite(0, Color.black);
        }
    }

    /**
     * Setzt den übergebenen Knoten an eine bestimmte Position.
     *
     * @param k           Knoten der gesetzt werden soll. Wenn <code>null</code> wird nichts gesetzt.
     * @param position    Position, an die der Knoten gesetzt werden soll. Wenn die Position außerhalb der Liste
     *                    liegt, wird der Knoten einfach angefügt.
     * @param setzeKanten Bestimmt, ob alle Kanten des Knotens gesetzt werden sollen, deren zweiter
     *                    Knoten bereits gesetzt wurde. Die Kanten werden auf die erste Seite gelegt.
     */
    public void knotenVerschieben(Knoten k, int position, boolean setzeKanten) {
        if (k == null)
            return;

        knoten.remove(k);

        if (position >= 0 && position < knoten.size())
            knoten.add(position, k);

        else
            knoten.add(k);

        if (setzeKanten)
            k.getKanten().stream().filter(kante -> kante.getNachbarn(k).isGesetzt()).forEach(kante -> kante.setSeite(seiten[0]));
    }

    /**
     * Erzeugt einen neuen Knoten mit einer freien Knotennummer.
     *
     * @return Gibt neuen Knoten zurueck oder <code>null</code>, wenn der Graph nicht editierbar ist.
     */
    public Knoten knotenErzeugen() {
        return new Knoten(findeFreieKnotennummer());

    }

    /**
     * @return seiten
     */
    public Seite[] getSeiten() {
        return seiten;
    }

    /**
     * Fuegt einen Knoten hinzu und sortiert die Liste.
     *
     * @param k neuer Knoten. Wenn <code>null</code> wird nichts hinzugefuegt.
     */
    public void knotenHinzufuegen(Knoten k) {
        if (k != null)
            knoten.add(k);
    }

    /**
     * Loescht den uebergebenen Knoten.
     *
     * @param k Knoten zum loeschen. Wenn <code>null</code> wird nichts geloescht.
     * @return Liste mit Kanten, die mit dem Knoten geloescht wurden oder leere Liste.
     */
    public Collection<Kante> knotenLoeschen(Knoten k) {
        Collection<Kante> kanten = new LinkedList<>();
        if (k != null) {
            kanten.addAll(k.loeschen());
            knoten.remove(k);
        }

        return kanten;
    }

    /**
     * Loescht den uebergebenen Knoten ohne die Kanten zu dereferenzieren.
     *
     * @param k Knoten zum loeschen. Wenn <code>null</code> wird nichts geloescht.
     */
    public void knotenEntfernen(Knoten k) {
        if (k == null)
            return;

        knoten.remove(k);
        for (Kante kante : k.getKanten())
            kante.setSeite(null);
    }

    /**
     * Verschiebt eine Kante auf eine bestimmte Seite oder dereferenziert die Kante, wenn die Seite <code>null</code> ist.
     *
     * @param k Kante, die verschoben wird.
     * @param s Seite, auf die die Kante verschoben werden soll. Wenn <code>null</code> wird die Kante dereferennziert.
     */
    public void kanteVerschieben(Kante k, Seite s) {
        if (k != null)
            k.setSeite(s);
    }

    /**
     * Verschiebt eine Kante auf eine bestimmte Seite.
     *
     * @param k     Kante, die verschoben wird.
     * @param seite Seitennummer, auf die die Kante verschoben werden soll. Wenn eine ungueltige Nummer uebergeben wird, wird die Kanten nicht verschoben.
     */
    public void kanteVerschieben(Kante k, int seite) {
        if (seite >= 0 && seite < seiten.length)
            k.setSeite(seiten[seite]);
    }

    /**
     * Fuegt eine Kante Hinzu und setzt dazu die Referenzen der Knoten und Seiten auf die Kante entsprechend der Knoten und Seiten, auf die die Kante referenziert.
     *
     * @param k Kante, die hinzugefuegt werden soll. Wenn <code>null</code> wird nichts hinzugefuegt.
     */
    public void kanteHinzufuegen(Kante k) {
        if (k == null)
            return;

        for (Knoten aKnoten : k.getKnoten()) {
            aKnoten.getKanten().add(k);
        }
        k.getSeite().getKanten().add(k);
    }

    /**
     * Loescht eine Kante indem alle Referenzen auf die Kante entfernt werden.
     *
     * @param k Kante, die geloescht werden soll. Wenn <code>null</code> wird nichts unternommen.
     */
    public void kanteLoeschen(Kante k) {
        if (k != null)
            k.loescheKante();
    }

    /**
     * Gibt die Seite zurueck, welche die uebergebene Kante enthaelt.
     *
     * @param k Kante
     * @return Seite, die auf die Kante referenziert oder <code>null</code>.
     */
    public Seite getSeiteMitKante(Kante k) {
        for (Seite s : seiten)
            if (s.enthaeltKante(k))
                return s;

        return null;
    }

    /**
     * Erzeugt eine neue Kante mit Hilfe eines Auswahlfensters.
     *
     * @return Neue Kante oder <code>null</code>, wenn die Auswahl abgebrochen wurde.
     */
    public Kante kanteErzeugen() {
        JComboBox<Knoten> startKnoten = new JComboBox<>();
        JComboBox<Knoten> endKnoten = new JComboBox<>();
        JComboBox<Seite> seite = new JComboBox<>();

        for (Seite s : seiten)
            seite.addItem(s);

        for (Knoten k : knoten) {
            startKnoten.addItem(k);
            endKnoten.addItem(k);
        }
        endKnoten.setSelectedIndex(1);

        JPanel panel = new JPanel(new GridLayout(3, 2, 5, 5));

        /* Knoten werden nur abgefragt, wenn es mehr als zwei gibt */
        if (knoten.size() > 2) {
            panel.add(new JLabel("Node 1"));
            panel.add(startKnoten);
            panel.add(new JLabel("Node 2"));
            panel.add(endKnoten);
        }

        /* Seiten werden nur abgefragt, wenn es mehr als eine Seite gibt */
        if (seiten.length > 1) {
            panel.add(new JLabel("Page"));
            panel.add(seite);
        }

        if (knoten.size() <= 2 && seiten.length == 1)
            return new Kante(knoten.getFirst(), knoten.getLast(), seiten[0]);

        String[] options = {"New Edge", "Cancel"};

        int auswahl = 0;

        while (auswahl != 1) {
            auswahl = JOptionPane.showOptionDialog(null, panel, "Choose Nodes for Edge", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, 1);

            if (auswahl == 0 && !startKnoten.getSelectedItem().equals(endKnoten.getSelectedItem()))
                break;
        }

        if (auswahl == 0)
            return new Kante((Knoten) startKnoten.getSelectedItem(), (Knoten) endKnoten.getSelectedItem(), (Seite) seite.getSelectedItem());

        return null;
    }

    /**
     * Erzeugt eine neue Seite mit einem Auswahldialog fuer dessen Farbe.
     *
     * @param farbwahl Legt fest, ob ein Farbauswahldialog gezeigt werden soll.
     * @return neue Seite.
     */
    public Seite seiteErzeugen(boolean farbwahl) {
        Color farbe = MyColors.get(findeFreieSeitennnummer());
        Color neu = null;

        if (farbwahl)
            neu = JColorChooser.showDialog(null, "Choose color", farbe);

        if (neu != null)
            farbe = neu;

        return new Seite(findeFreieSeitennnummer(), farbe);
    }

    /**
     * Fuegt eine Seite an eine bestimmte Stelle ein.
     *
     * @param s     Seite, die einzufuegen ist. Wenn <code>null</code> wird nichts ausgefuehrt.
     * @param index Position, an der die Seite eingefuegt werden soll. Ist die Position außerhalb des Arrays, wird die Seite am Ende eingefuegt.
     */
    public void seiteHinzufuegen(Seite s, int index) {
        if (s == null)
            return;

        if (index < 0 || index > seiten.length)
            index = seiten.length;

        Seite[] tmp = seiten;
        seiten = new Seite[tmp.length + 1];

        int neueSeitenIndex = 0;
        for (Seite aTmp : tmp) {
            seiten[neueSeitenIndex++] = aTmp;

            if (neueSeitenIndex == index)
                neueSeitenIndex++;
        }

        seiten[index] = s;
    }

    /**
     * Erzeugt ein Fenster zur Auswahl von bis zu zwei Seiten. Die erste Seite wird geloescht, deren Kanten auf die zweite Seite verschoben oder ebenfalls geloescht, wenn keine zweite Kante ausgewaehlt wurde.
     *
     * @return Array mit zwei Seiten. Ist die erste Seite <code>null</code>, wurde die Auswahl abgebrochen. Ist die zweite Seite <code>null</code>, sollen die Kanten geloescht werden.
     */
    public Seite[] seitenAuswahl() {
        JComboBox<Object> seiteZuLoeschen = new JComboBox<>();
        JComboBox<Object> seiteZuVerschieben = new JComboBox<>();

        Seite[] auswahlSeiten = new Seite[2];

        seiteZuVerschieben.addItem("None");

        for (Seite s : seiten) {
            seiteZuLoeschen.addItem(s);
            seiteZuVerschieben.addItem(s);
        }

        JPanel panel = new JPanel(new GridLayout(1, 2, 5, 5));
        panel.add(new JLabel("Page to delete"));
        panel.add(seiteZuLoeschen);
        panel.add(new JLabel("Move Edges to page"));
        panel.add(seiteZuVerschieben);

        String[] options = {"Delete Page", "Cancel"};

        int auswahl = 0;

        while (auswahl != 1) {
            auswahl = JOptionPane.showOptionDialog(null, panel, "Choose Page to delete", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, 1);

            if (auswahl == 0 && !seiteZuLoeschen.getSelectedItem().equals(seiteZuVerschieben.getSelectedItem()))
                break;
        }

        if (auswahl == 0) {
            auswahlSeiten[0] = (Seite) seiteZuLoeschen.getSelectedItem();

            if (!seiteZuVerschieben.getSelectedItem().toString().equals("None"))
                auswahlSeiten[1] = (Seite) seiteZuVerschieben.getSelectedItem();
        }

        return auswahlSeiten;
    }

    /**
     * Loescht eine Seite und verschiebt deren Kanten auf eine andere Seite.
     *
     * @param seitenLoeschen              Seite, die geloescht werden soll. Wenn <code>null</code> wird nichts unternommen.
     * @param seiteFuerKantenverschiebung Seite, auf die die Kanten verschoben werden sollen. Wenn <code>null</code> werden die Kanten nur geloescht.
     * @return Gibt eine Liste mit verschobenen oder geloeschten Kanten zurueck. Ist seitenLoeschen <code>null</code>, wird eine leere Liste zurueckgegeben.
     */
    public Collection<Kante> seiteLoeschen(Seite seitenLoeschen, Seite seiteFuerKantenverschiebung) {
        if (seitenLoeschen == null)
            return new LinkedList<>();

        Seite[] tmp = seiten;
        seiten = new Seite[tmp.length - 1];

        int indexNeueSeite = 0;
        for (Seite aTmp : tmp)
            if (aTmp != seitenLoeschen)
                seiten[indexNeueSeite++] = aTmp;

        return seitenLoeschen.loeschen(seiteFuerKantenverschiebung);
    }

    /**
     * Gibt die Position einer Seite zurueck.
     *
     * @param s zu suchende Seite
     * @return Position oder -1
     */
    public int getIndexDerSeite(Seite s) {
        for (int i = 0; i < seiten.length; i++)
            if (seiten[i] == s)
                return i;

        return -1;
    }

    /**
     * Durchsucht die Knotenliste nach einer freien Knotennummer und gibt diese zurueck.
     *
     * @return kleinste, ungenutzte Knotennummer groeßer 0
     */
    private int findeFreieKnotennummer() {
        int nummer = 0;
        int freieNummer = 1;

        while (nummer != freieNummer) {
            freieNummer = nummer;
            for (Knoten k : knoten)
                if (k.getNummer() == nummer)
                    nummer++;
        }

        return nummer;
    }

    /**
     * Durchsucht die Knotenliste nach einer freien Seitennummer und gibt diese zurueck.
     *
     * @return kleinste, ungenutzte Seitennummer groeßer 0
     */
    private int findeFreieSeitennnummer() {
        int nummer = 0;
        int freieNummer = 1;

        while (nummer != freieNummer) {
            freieNummer = nummer;
            for (Seite s : seiten)
                if (s.getNummer() == nummer)
                    nummer++;
        }

        return nummer;
    }

    /**
     * Sortiert die Knoten des Graphen entsprechend der Koordinaten ihrer
     * Mittelpunkte und zaehlt die Kreuzungen der Kanten.
     */
    public void knotenSortieren() {
        Collections.sort(knoten);
    }

    /**
     * Gibt den Knoten zurück, dessen Kreis die angegebenen Koordinaten enthält.
     *
     * @param x x-Koordinate
     * @param y y-Koordinate
     * @return markierter Knoten oder <code>null</code>
     */
    public Knoten getGeklicktenKnoten(int x, int y) {
        for (Knoten k : knoten)
            if (k.getKreis().isInside(x, y))
                return k;
        return null;
    }

    /**
     * Berechnet die Kreise des Graphen
     *
     * @param height       Höhe der Darstellung
     * @param width        Breite der Darstellung
     * @param anteilRadius Relativer Radius in Bezug auf die Darstellungsfläche
     */
    public void berechneDarstellungKnoten(int height, int width, int anteilRadius) {
        int index = 0;
        for (Knoten k : this.knoten) {
            k.getKreis().berechneKoordinaten(index++, height, width, anteilRadius, this.knoten.size());
        }

        zaehleKreuzungen();
    }

    /**
     * Zeichnet die Knoten des Graphen
     *
     * @param g Graph, in den gezeichnet wird
     */
    public void zeichneKnoten(Graphics2D g) {
        // Zeichne Knoten
        for (Knoten k : knoten)
            k.getKreis().zeichne(g);
    }

    /**
     * Gibt des Index einen Knoten zurück, oder -1
     *
     * @param k Knoten
     * @return Index des Knotens oder -1
     */
    public int getIndexDesKnoten(Knoten k) {
        return this.knoten.indexOf(k);
    }

    /**
     * Die Funktion speichert die übergebenen Arrays im richtigem Format in eine
     * ausgewaehlte Datei.
     *
     * @return 0 = nicht gespeichert, 1 = gespeichert, -1 = fehler
     */
    public int speichern() {
        return IO.speichern(knoten, seiten);
    }

    /**
     * Gibt die Anzahl der Knoten zurueck.
     *
     * @return Anzahl der Knoten
     */
    public int anzahlKnoten() {
        return knoten.size();
    }

    /**
     * Gibt die Anzahl der Seiten zurueck.
     *
     * @return Anzahl der Seiten
     */
    public int anzahlSeiten() {return seiten.length;}

    /**
     * Bewegt den Kreis einen Knotens
     *
     * @param k Knoten, der bewegt wird.
     * @param x x-Koordinate des Mittelpunktes des Kreises.
     * @param y y-Koordinate des Mittelpunktes des Kreises.
     */
    public void bewegeKnoten(Knoten k, int x, int y) {
        k.getKreis().bewege(x, y);
    }

    /**
     * Zeichnet die Kanten der uebergebenen Seite.
     *
     * @param s          Seite. Wenn <code>null</code> wird nichts gezeichnet.
     * @param graphics2D Graphic auf der gezeichnet werden soll.
     * @param oben       <code>true</code>: Seite wird oben gezeichnet; <code>false</code> Seite wird unten gezeichnet.
     */
    public void zeichneSeite(Seite s, Graphics2D graphics2D, boolean oben) {
        if (s != null)
            s.zeichneKanten(graphics2D, oben);
    }

    /**
     * Berechnet die Darstellung der Boegen einer Seite entsprechend der Zeichenhaelfte auf der sie angezeigt werden soll und der Hoehe der Zeichenflaeche.
     *
     * @param s      Seite. Wenn <code>null</code> wird nichts durchgefuehrt.
     * @param height Hoehe der Zeichenflaeche.
     * @param width  Breite der Zeichenflaeche.
     */
    public void berechneDarstellungBoegen(Seite s, int height, int width) {
        if (s != null)
            s.berechneBoegen(height, width);
        zaehleKreuzungen();
    }

    /**
     * Legt fest, ob eine Kante hervorzuheben ist.
     *
     * @param k           Kante
     * @param hervorheben <code>true</code>: Kante wird hervorgehoben; <code>false</code> Kante wird nicht hervorgehoben.
     */
    public void kanteHervorheben(Kante k, boolean hervorheben) {
        if (k == null)
            return;

        k.getArc().setHervorheben(hervorheben);
    }

    /**
     * Bestimmt alle Seiten, die eine Kante nicht enthalten.
     *
     * @param k Kante
     * @return Liste mit Seiten, die k nicht enthalten.
     */
    public Iterable<Seite> seitenDieKanteNichtEnthalen(Kante k) {
        Collection<Seite> seiten = new LinkedList<>();

        if (k != null)
            for (Seite seite : this.seiten) {
                if (!seite.enthaeltKante(k))
                    seiten.add(seite);
            }

        return seiten;
    }

    /**
     * Zaehlt die Kreuzungen im Graphen und setzt sie auf das Label.
     *
     * @return Gibt die Anzahl der Kreuzungen zurueck.
     */
    int zaehleKreuzungen() {
        int kreuzungen = 0;
        for (Seite s : getSeiten()) {
            ListIterator<Kante> ki1 = s.getKanten().listIterator();
            while (ki1.hasNext()) {
                Kante k1 = ki1.next();
                ListIterator<Kante> ki2 = s.getKanten().listIterator(ki1.nextIndex());
                while (ki2.hasNext()) {
                    Kante k2 = ki2.next();
                    if (k1.schneidetKante(k2))
                        kreuzungen++;
                }
            }
        }

        if (this.labelKreuzungen != null)
            this.labelKreuzungen.setText(" crossing: " + kreuzungen);

        return kreuzungen;
    }

    /**
     * Setzt das JLabel, in dem die Anzahl an Kreuzungen eingetragen werden soll.
     *
     * @param kreuzungen JLabel zur Anzeige der Anzahl der Kreuzungen.
     */
    public void setLabelKreuzungen(JLabel kreuzungen) {
        this.labelKreuzungen = kreuzungen;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("# Number of Nodes (N)");
        s.append(System.lineSeparator());
        s.append(knoten.size());
        s.append(System.lineSeparator());

        s.append("# Number of pages (K)");
        s.append(System.lineSeparator());
        s.append(seiten.length);
        s.append(System.lineSeparator());

        s.append("# Permutation of nodes");
        s.append(System.lineSeparator());
        for (Knoten k : knoten) {
            s.append(k);
            s.append(System.lineSeparator());
        }

        s.append("# Edges with starting node, ending node and assigned page in rectangular brackets");
        s.append(System.lineSeparator());
        for (Seite aSeiten : seiten) {
            for (int j = 0; j < aSeiten.getKanten().size(); j++) {
                s.append(aSeiten.getKanten().get(j));
                s.append(System.lineSeparator());
            }
        }

        return s.toString();
    }

    Deque<Knoten> getKnoten() {
        return knoten;
    }

    public int schwerPunkt() {
        return (int) ((getKnoten().getFirst().getKreis().getMittelpunkt().getX() + getKnoten().getLast().getKreis().getMittelpunkt().getX()) / 2);
    }
}
