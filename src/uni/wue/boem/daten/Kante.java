/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie k�nnen es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder sp�teren
 *     ver�ffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es n�tzlich sein wird, aber
 *     OHNE JEDE GEW�HRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gew�hrleistung der MARKTF�HIGKEIT oder EIGNUNG F�R EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License f�r weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem.daten;

import uni.wue.boem.elemente.Arc;

/**
 * Klasse repraesentiert eine Kante im Graphen und entaehlt Informationen darueber, wie die Kante zu zeichnen ist.
 *
 * @author Dennis van der Wals
 */
public class Kante {
    private Knoten[] knoten;
    private Seite seite;
    private Arc arc;

    /**
     * Konstuktor. Die Reihenfolge der Knoten ist unerheblich, sie duerfen jedoch nicht <code>null</code> sein.
     *
     * @param a Erster Knoten.
     * @param b Zweiter Knoten.
     * @param s Seite, auf der die Kante liegt. Wenn <code>null</code>, wird die Kante auf keine Seite gelegt.
     */
    public Kante(Knoten a, Knoten b, Seite s) {
        if (a == null || b == null)
            throw new IllegalArgumentException("Vertex a and b must not be NULL.");

        this.knoten = new Knoten[2];
        this.knoten[0] = a;
        this.knoten[1] = b;

        for (Knoten aKnoten : knoten) {
            aKnoten.getKanten().add(this);
        }

        this.seite = s;
        if (s != null)
            s.getKanten().add(this);

        this.arc = new Arc(a, b);
    }

    public Knoten getNachbarn(Knoten k) {
        if (k == null)
            return null;

        if (k.equals(knoten[0]))
            return knoten[1];

        else if (k.equals(knoten[1]))
            return knoten[0];

        else
            return null;
    }

    /**
     * @return the seite
     */
    Seite getSeite() {
        return seite;
    }

    /**
     * @param seite the seite to set
     */
    void setSeite(Seite seite) {
        if (this.seite != null)
            this.seite.getKanten().remove(this);

        this.seite = seite;

        if (this.seite != null)
            this.seite.getKanten().add(this);
    }

    /**
     * @return the knoten
     */
    Knoten[] getKnoten() {
        return knoten;
    }

    public String toString() {
        return String.format("%s %s [%s]", (knoten[0].getNummer() < knoten[1].getNummer()) ? knoten[0] : knoten[1], (knoten[0].getNummer() > knoten[1].getNummer()) ? knoten[0] : knoten[1], seite);
    }

    /**
     * @return the arc
     */
    public Arc getArc() {
        return arc;
    }

    /**
     * Loescht die Kante, indem sie von der Seite und den Knoten dereferenziert wird.
     */
    void loescheKante() {
        if (seite != null)
            seite.entferneKante(this);

        for (Knoten k : knoten)
            k.entferneKante(this);
    }

    /**
     * Bestimmt, ob zwei Kanten sich schneiden.
     *
     * @param k Kante, die geprueft werden soll. Wenn <code>null</code> wird <code>false</code> zurueckgegeben.
     * @return Wahrheitswert, ob die Kanten sich schneiden.
     */
    public boolean schneidetKante(Kante k) {
        if (k == null || k.getSeite() != this.getSeite())
            return false;

        Knoten[] sortiertThis = new Knoten[2];
        sortiertThis[0] = knoten[0];
        sortiertThis[1] = knoten[1];
        if (knoten[1].compareTo(knoten[0]) == -1) {
            sortiertThis[0] = knoten[1];
            sortiertThis[1] = knoten[0];
        }

        Knoten[] sortiertk = new Knoten[2];
        sortiertk[0] = k.knoten[0];
        sortiertk[1] = k.knoten[1];
        if (k.knoten[1].compareTo(k.knoten[0]) == -1) {
            sortiertk[0] = k.knoten[1];
            sortiertk[1] = k.knoten[0];
        }

        return ((sortiertk[1].compareTo(sortiertThis[0]) + sortiertThis[1].compareTo(sortiertk[0]) == 2) && sortiertThis[0].compareTo(sortiertk[0]) == sortiertThis[1].compareTo(sortiertk[1]));
    }

    /**
     * Gibt die Anzahl der Kanten zurueck, die diese Kante schneiden.
     *
     * @return Anzahl der Kanten, die diese Kante schneiden.
     */
    int zaehleKreuzendeKanten() {
        if (seite == null)
            return 0;

        int kreuzendeKanten = 0;

        for (Kante k : seite.getKanten())
            if (k != this && this.schneidetKante(k))
                kreuzendeKanten++;

        return kreuzendeKanten;
    }

    int schwerPunkt() {
        return (int) ((knoten[0].getKreis().getMittelpunkt().getX() + knoten[1].getKreis().getMittelpunkt().getX()) / 2);
    }
}
