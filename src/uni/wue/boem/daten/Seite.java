/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie können es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 *     veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es nützlich sein wird, aber
 *     OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License für weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem.daten;

import uni.wue.boem.elemente.ColorIcon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.LinkedList;

/**
 * Die Klasse repraesentiert eine Seite im Graphen fuer die Aufnahme und Darstellung von Kanten.
 *
 * @author Dennis van der Wals
 */
public class Seite {
    private int nummer;
    private Color farbe;
    private LinkedList<Kante> kanten;

    private LinkedList<JButton> farbButtons;
    private ActionListener farbWahlAktion;

    public Seite(int nummer, Color farbe) {
        if (nummer < 0)
            throw new IllegalArgumentException("The label number of a page must be non-negative.");

        this.nummer = nummer;
        this.farbe = farbe;
        kanten = new LinkedList<>();
    }

    /**
     * @return the nummer
     */
    int getNummer() {
        return nummer;
    }

    /**
     * @return the farbe
     */
    public Color getFarbe() {
        return farbe;
    }

    /**
     * @return the kanten
     */
    public java.util.List<Kante> getKanten() {
        return kanten;
    }

    public String toString() {
        return nummer + "";
    }

    boolean enthaeltKante(Kante k) {
        return kanten.contains(k);
    }

    /**
     * Entfernt eine Kante
     *
     * @param k Kante, die entfernt werden soll
     */
    void entferneKante(Kante k) {
        if (k != null)
            this.kanten.remove(k);
    }

    public JButton getFarbButton() {
        if (farbButtons == null)
            farbButtons = new LinkedList<>();

        JButton neuerFarbButton = new JButton(new ColorIcon(farbe));
        neuerFarbButton.addActionListener(getFarbWahlAktion());

        farbButtons.add(neuerFarbButton);

        return neuerFarbButton;
    }

    /**
     * @return the farbWahlAktion
     */
    private ActionListener getFarbWahlAktion() {
        if (farbWahlAktion == null)
            farbWahlAktion = e -> {
                Color neu = JColorChooser.showDialog(null, "Choose color", getFarbe());

                if (neu != null) {
                    farbe = neu;
                    
                    MyColors.set(nummer, neu);

                    for (JButton jButton : farbButtons) {
                        jButton.setIcon(new ColorIcon(farbe));
                    }
                }
            };
        return farbWahlAktion;
    }

    Collection<Kante> loeschen(Seite verschiebeKanten) {
        Collection<Kante> kanten = new LinkedList<>();
        kanten.addAll(this.kanten);
        kanten.forEach(kante -> kante.setSeite(verschiebeKanten));

        return kanten;
    }

    /**
     * Zeichnet die Kanten der Seite.
     *
     * @param g    Graphic, auf die gezeichnet werden soll
     * @param oben <code>true</code>: Seite wird oben gezeichnet; <code>false</code> Seite wird unten gezeichnet.
     */
    void zeichneKanten(Graphics2D g, boolean oben) {
        if (kanten != null)
            for (Kante kante : kanten)
                kante.getArc().draw(g, farbe, oben);
    }

    /**
     * Setzt die Zeichenhaelft aller entahltenen Boegen und berechnet die Koordinaten.
     *
     * @param height Hoehe der Zeichenflaeche.
     * @param width  Breite der Zeichenflaeche
     */
    void berechneBoegen(int height, int width) {
        if (kanten != null)
            for (Kante kante : kanten) {
                kante.getArc().berechneKoordinaten(height, width);
            }
    }
}