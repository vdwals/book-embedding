/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie können es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 *     veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es nützlich sein wird, aber
 *     OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License für weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem.daten;

import uni.wue.boem.GraphGUIManager;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;

/**
 * Dies Klasse stellt zwei Varianten zum loesen der Einbettung bereit. Das bedeutet, dass die Kanten eines Graph in der
 * Buchdarstellung auf so viele Seiten wie nötig verteilt werden, um eine Bucheinbettung darzustellen. Dazu können
 * auch Knoten verschoben werden.
 *
 * @author Dennis van der Wals
 */
public class Loeser {
    private final Graph g;
    private final GraphGUIManager guiManager;

    boolean symmetrisch = false;

    /**
     * Stellt einen Loeser bereit fuer einen uebergebenen Graphen.
     *
     * @param g          zu loesender Graph.
     * @param guiManager GUI Manager fuer die Darstellung des Graphen.
     */
    public Loeser(Graph g, GraphGUIManager guiManager) {
        this.g = g;
        this.guiManager = guiManager;
    }

    /**
     * Verabeitet eine Liste von Kanten und verteilt diese im Graphen, sodass sie keine anderen Kanten im Graphen
     * schneiden.
     *
     * @param liste      Liste mit zu verteilenden Kanten.
     * @param comparator Komparator, der die Priorität der Kanten festlegt.
     * @param g          Graph, in dem die Kanten verteilt werden sollen.
     * @param guiManager GUI Manager fuer die Darstellung des Graphen.
     */
    private static void verteileKanten(LinkedList<Kante> liste, Comparator<Kante> comparator, Graph g, GraphGUIManager guiManager) {
        while (!liste.isEmpty()) {
            Collections.sort(liste, comparator);
            Kante naechsteKante = liste.poll();

            int seitenIndex = 1;

            while (naechsteKante.zaehleKreuzendeKanten() > 0) {
                /* Erzeuge eine neue Seite, wenn die bisherigen nicht ausreichen */
                if (g.anzahlSeiten() < seitenIndex + 1) {
                    g.seiteHinzufuegen(g.seiteErzeugen(false), seitenIndex);
                }

                g.kanteVerschieben(naechsteKante, seitenIndex);

                aktualisiereDarstellung(guiManager);

                seitenIndex++;
            }
        }
    }

    /**
     * Aktualisiert die Darstellung des Graphen.
     *
     * @param manager GUI Manager fuer die Darstellung der Graphen.
     */
    private static void aktualisiereDarstellung(GraphGUIManager manager) {
        manager.aktualisiereDarstellung();
        manager.aktualisiereLeisten();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Verteilt die Kanten im Graphen um vorhandene Kreuzungen aufzuheben.
     */
    private void verteileKanten() {
        if (g == null || g.zaehleKreuzungen() == 0)
            return;

        guiManager.blockiereGUI(true);

        /* Fuelle Prioritaetenwarteschlange mit Kanten */
        Comparator<Kante> comparator = new PrioritaetFuerKanten();
        LinkedList<Kante> liste = new LinkedList<>();

        for (Seite s : g.getSeiten())
            liste.addAll(s.getKanten());

        verteileKanten(liste, comparator, g, guiManager);

        guiManager.blockiereGUI(false);
    }

    /**
     * Versucht die Anzahl der Seiten des Graphen zu minimieren, indem alle Kanten auf die erste Seite verschoben
     * werden und anschließend verteilt.
     */
    public void minimiereSeiten() {
        if (g == null)
            return;

        guiManager.blockiereGUI(true);

        /* Verschiebe alle Kanten auf Ausgangsposition und loese dann mit Kantenverteilung */
        while (g.getSeiten().length > 1) {
            g.seiteLoeschen(g.getSeiten()[1], g.getSeiten()[0]);
        }

        aktualisiereDarstellung(guiManager);

        verteileKanten();
    }

    /**
     * Ordnet die Knoten und Verteilt die Kanten so, dass keine Kreuzungen auf möglichst wenigen Seiten entstehen.
     */
    public void loeseMitHeuristik() {
        if (g == null)
            return;

        guiManager.blockiereGUI(true);

        /* Fuelle Prioritaetenwarteschlange mit Knoten */
        Comparator<Knoten> comparator = new PrioritaetFuerKnoten();
        PriorityQueue<Knoten> queue = new PriorityQueue<>(g.anzahlKnoten(), comparator);

        /* Setze Status der Knoten zurueck */
        for (Knoten k : g.getKnoten())
            k.setGesetzt(false);

        queue.addAll(g.getKnoten());
        queue.forEach(g::knotenEntfernen);

        aktualisiereDarstellung(guiManager);

        /* Platziere die ersten Knoten */
        Knoten k = queue.remove();
        k.setGesetzt(true);
        g.knotenVerschieben(k, 0, false);

        aktualisiereDarstellung(guiManager);

        k = queue.remove();
        k.setGesetzt(true);
        g.knotenVerschieben(k, 1, true);

        aktualisiereDarstellung(guiManager);

        while (!queue.isEmpty()) {
            /* Setze den naechsten Knoten an den Anfang und bestimme die Anzahl der Krezungen. */
            Knoten naechsterKnoten = queue.remove();
            naechsterKnoten.setGesetzt(true);
            g.knotenVerschieben(naechsterKnoten, 0, true);

            aktualisiereDarstellung(guiManager);

            int kreuzungen = g.zaehleKreuzungen();

            /* Verschiebe den Knoten an das Ende. */
            g.knotenVerschieben(naechsterKnoten, -1, false);
            int kreuzungenVariante2 = g.zaehleKreuzungen();

            /* Ist die Anzahl an Kreuzungen groeßer als zuvor, Verschiebe den Knoten wieder an den Anfang */
            if (kreuzungenVariante2 >= kreuzungen)
                g.knotenVerschieben(naechsterKnoten, 0, false);

            else {
                kreuzungen = kreuzungenVariante2;
                aktualisiereDarstellung(guiManager);
            }


            /* Loese bestehende Kreuzungen auf */
            if (kreuzungen > 0) {
                /* Erstelle eine Liste mit Kanten, die sich Kreuzungen erzeugen */
                LinkedList<Kante> kreuzendeKanten = naechsterKnoten.kreuzendeKanten();
                verteileKanten(kreuzendeKanten, new PrioritaetFuerKanten(), g, guiManager);
            }
        }

        guiManager.blockiereGUI(false);
    }

    /**
     * Diese Klasse dient als Comparator fuer eine Prioritaetenwarteschlange. Die Prioritaet eines Knotens vor einem
     * anderem wird bestimmt durch die kleinste Anzahl an Nachbarn, die eine offene Verbindung besitzen.
     */
    private class PrioritaetFuerKnoten implements Comparator<Knoten> {

        @Override
        public int compare(Knoten o1, Knoten o2) {
            if (o2 == null)
                return 1;

            if (o1 == null)
                return -1;

            int prio1 = o1.nichtGesetzteNachbarnMitOffenenKanten();
            int prio2 = o2.nichtGesetzteNachbarnMitOffenenKanten();

            if (prio1 > prio2)
                return -1;

            else if (prio1 < prio2)
                return 1;

            return 0;
        }
    }

    /**
     * Diese Klasse dient als Comparator fuer eine Prioritaetenwarteschlange. Die Prioritaet einer Kante vor einer
     * anderen wird bestimmt durch die kleinste Anzahl an Schnitten die sie erzeugt.
     */
    private class PrioritaetFuerKanten implements Comparator<Kante> {

        @Override
        public int compare(Kante o1, Kante o2) {
            if (o2 == null)
                return 1;

            if (o1 == null)
                return -1;

            int prio1 = 0;
            int prio2 = 0;

            if (symmetrisch) {
                int kreuzungen1 = o1.zaehleKreuzendeKanten();
                if (kreuzungen1 == 0)
                    return 1;

                int kreuzungen2 = o2.zaehleKreuzendeKanten();
                if (kreuzungen2 == 0)
                    return -1;

                prio1 = Math.abs(o1.schwerPunkt() - g.schwerPunkt());
                prio2 = Math.abs(o2.schwerPunkt() - g.schwerPunkt());
            }

            if (!symmetrisch || prio1 == prio2) {
                prio1 = o1.zaehleKreuzendeKanten();
                prio2 = o2.zaehleKreuzendeKanten();
            }

            if (prio1 > prio2)
                return -1;

            else if (prio1 < prio2)
                return 1;

            return 0;
        }
    }
}
