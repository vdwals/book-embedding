package uni.wue.boem.daten;

import java.awt.*;

public abstract class MyColors {
    private static final String[] ColorValues = new String[]{"000000", "BC364C", "57DA90", "3694F5", "BD7C37", "FF00FF", "00FFFF",
            "800000", "008000", "000080", "808000", "800080", "008080", "808080", 
            "C00000", "00C000", "0000C0", "C0C000", "C000C0", "00C0C0", "C0C0C0", 
            "400000", "004000", "000040", "404000", "400040", "004040", "404040", 
            "200000", "002000", "000020", "202000", "200020", "002020", "202020", 
            "600000", "006000", "000060", "606000", "600060", "006060", "606060", 
            "A00000", "00A000", "0000A0", "A0A000", "A000A0", "00A0A0", "A0A0A0", 
            "E00000", "00E000", "0000E0", "E0E000", "E000E0", "00E0E0", "E0E0E0", 
        };
    
    public static Color get(int i) {
    	if (i < ColorValues.length)
    		return Color.decode("#"+ColorValues[i]);
    	else
    		return new Color((int)(Math.random() * 0x1000000));
    }
    
    public static void set(int i, Color color) {
    	String value = Integer.toHexString(color.getRed()) + Integer.toHexString(color.getGreen()) + Integer.toHexString(color.getBlue());
    	if (i < ColorValues.length)
    		ColorValues[i] = value;
    }

}
