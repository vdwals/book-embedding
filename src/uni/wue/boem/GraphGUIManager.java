/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie können es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 *     veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es nützlich sein wird, aber
 *     OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License für weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem;

import uni.wue.boem.aktionen.AktionManager;
import uni.wue.boem.daten.Graph;

import javax.swing.*;
import java.util.Deque;
import java.util.LinkedList;

/**
 * Klasse fuer die Verwaltung und Anzeige mehrerer GraphGUIs mit simultaner Bearbeitung.
 *
 * @author Dennis van der Wals
 */
public class GraphGUIManager {
    private final MainGUI mainGUI;
    private LinkedList<GraphGUI> graphGUIs;
    /* Daten Elemente */
    private Graph graph;
    private int anteilRadius;

    public GraphGUIManager(MainGUI mainGUI) {
        this.mainGUI = mainGUI;
    }

    /**
     * Aktualisiert die Darstellung der Seitenleisten.
     */
    public void aktualisiereLeisten() {
        graphGUIs.forEach(uni.wue.boem.GraphGUI::aktualisiereLeisten);
    }

    /**
     * Setzt den Graphen fuer alle GUIs
     *
     * @param g Graph
     */
    void setGraph(Graph g) {
        this.graph = g;

        graphGUIs.stream().filter(gui -> gui != null).forEach(gui -> gui.setGraph(g));
    }

    /**
     * Setzt den AktionManager fuer alle Graphen
     *
     * @param aktionenManager AktionManager
     */
    void setAktionManager(AktionManager aktionenManager) {
        for (GraphGUI gui : graphGUIs)
            gui.setAktionManager(aktionenManager);
    }


    /**
     * Vergroeßert die Ansicht auf mehr Graphen
     *
     * @param aktionManager Aktionmanager
     */
    GraphGUI addGraphGui(AktionManager aktionManager) {
        GraphGUI neu = new GraphGUI(this, aktionManager);
        neu.getFrame().setVisible(true);
        neu.setGraph(graph);

        getGraphGUIs().add(neu);

        return neu;
    }


    /**
     * Gibt das Array der GraphGUIs zurueck.
     *
     * @return GraphGUIs
     */
    Deque<GraphGUI> getGraphGUIs() {

        if (graphGUIs == null)
            graphGUIs = new LinkedList<>();

        return graphGUIs;
    }

    /**
     * Setzt den Anteil des Radius der Kreise an der Gesamtflaeche.
     *
     * @param anteilRadius Anteil des Radius an Zeichenflaeche
     */
    void setAnteilRadius(int anteilRadius) {
        this.anteilRadius = anteilRadius;

        aktualisiereDarstellung();
    }

    /**
     * Loest alle Berechnungen aus, die fuer die korrekte Darstellung erforderlich
     * sind. Dazu zaehlen die Berechnungen der Mittelpunkte der Knoten sowie die
     * Kanten.
     */
    public void aktualisiereDarstellung() {
        berechneDarstellungKnoten(false);
        berechneDarstellungKanten(false);

        graphGUIs.stream().filter(gui -> gui != null).forEach(gui -> SwingUtilities.invokeLater(gui::repaint));
    }

    /**
     * Blockiert Eingaben auf der GUI, bis ein Prozess diese wieder freigibt.
     *
     * @param blockiere Gibt an, ob GUI blockiert oder freigegeben werden soll.
     */
    public void blockiereGUI(boolean blockiere) {
        mainGUI.setGuiBlockiert(blockiere);

        for (GraphGUI gui : graphGUIs)
            gui.setGuiBlockiert(blockiere);
    }

    /**
     * Fuert nur Neuberechnung und Zeichnung der Knoten aus. So koennen neue
     * Knotenwerte berechnet werden, ohne die Kanten zu beeinflussen.
     *
     * @param zeichnen Legt fest, ob nach der Berechnung gezeichnet werden soll.
     */
    public void berechneDarstellungKnoten(boolean zeichnen) {
        if (graph == null || graphGUIs.isEmpty())
            return;

        // Berechne die Kreise aller Knoten neu, die gezeichnet werden sollen.
        graph.berechneDarstellungKnoten(graphGUIs.getFirst().getHeight(), graphGUIs.getFirst().getWidth(), anteilRadius);

        if (zeichnen)
            for (int guiIndex = 1; guiIndex < graphGUIs.size(); guiIndex++) {
                SwingUtilities.invokeLater(graphGUIs.get(guiIndex)::repaint);
            }
    }

    /**
     * Setzt die transformationswerte für alle GUIs
     */
    public void setTransform() {
        if (graph == null || graphGUIs.isEmpty())
            return;

        GraphGUI first = graphGUIs.getFirst();
        for (int guiIndex = 1; guiIndex < graphGUIs.size(); guiIndex++) {
            GraphGUI gui = graphGUIs.get(guiIndex);
            gui.setTransform(first.getWidth(), first.getHeight());
            SwingUtilities.invokeLater(gui::repaint);
        }
    }

    /**
     * Fuert nur Neuberechnungen und Zeichnungen der Kanten aus.So koennen neue
     * Kantenwerte berechnet werden, ohne die Knoten zu beeinflussen.
     *
     * @param zeichnen Legt fest, ob nach der Berechnung gezeichnet werden soll.
     */
    public void berechneDarstellungKanten(boolean zeichnen) {
        if (graph == null)
            return;

        GraphGUI first = graphGUIs.getFirst();
        if (first != null)
            // Berechne die Kreise aller Kanten neu, die gezeichnet werden sollen.
            first.berechneDarstellungKanten(zeichnen);

        if (zeichnen)
            for (int guiIndex = 1; guiIndex < graphGUIs.size(); guiIndex++) {
                SwingUtilities.invokeLater(graphGUIs.get(guiIndex)::repaint);
            }
    }
}
