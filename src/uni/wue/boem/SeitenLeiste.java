/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie können es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 *     veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es nützlich sein wird, aber
 *     OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License für weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem;

import uni.wue.boem.daten.Graph;
import uni.wue.boem.daten.Seite;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Klasse fuer die Leisten zur Auswahl der anzuzeigenden Seiten.
 *
 * @author Dennis van der Wals
 */
class SeitenLeiste extends JPanel {
    /**
	 * 
	 */
	private static final long serialVersionUID = -8910715291618542989L;
	private final GraphGUI gui;
    private HashMap<JCheckBox, Seite> seiteProCheckbox;
    private LinkedList<Seite> aktiveSeiten;
    private ActionListener actionL;
    private boolean oben; 

    /**
     * Konstruktor.
     *
     * @param gui          Graphische Oberflaeche, deren Seiten ein- und ausgeschaltet werden sollen.
     * @param aktiveSeiten Liste mit aktivierten Seiten.
     */
    public SeitenLeiste(GraphGUI gui, LinkedList<Seite> aktiveSeiten, boolean oben) {
        if (gui == null)
            throw new IllegalArgumentException("The gui must not be NULL");

        this.gui = gui;
        this.aktiveSeiten = aktiveSeiten;
        this.oben = oben;
    }

    /**
     * Setzt den Graphen und erzeugt damit neue Seitenleisten mit entsprechenden Seiten.
     *
     * @param g                   Graph. Wenn <code>null</code> wird nichts ausgefuehrt.
     * @param aktiveSeiten        Liste mit aktiven Seiten
     * @param aktiviereNeueSeiten Legt fest, ob neue hinzugefügte Seiten direkt aktiviert werden
     *                            sollen.
     */
    public void setGraph(Graph g, LinkedList<Seite> aktiveSeiten, boolean aktiviereNeueSeiten) {
        if (g == null)
            return;

        this.aktiveSeiten = aktiveSeiten;
        LinkedList<Seite> seitenImGraph = new LinkedList<>();

        JPanel panel = new JPanel(new GridLayout(1, g.getSeiten().length + 1, 5, 5));

        int alteGroesse = 0;
        if (aktiviereNeueSeiten)
            alteGroesse = seiteProCheckbox.size();

        seiteProCheckbox = new HashMap<>();

        for (Seite seite : g.getSeiten()) {
            panel.add(erzeugePanelProSeite(seite, this.aktiveSeiten.contains(seite)));
            seitenImGraph.add(seite);
        }

        //Pruefe, ob aktive Seiten vorhanden sind, die nicht im Graphen sind
        for (int i = 0; i < aktiveSeiten.size(); i++)
            if (!seitenImGraph.contains(aktiveSeiten.get(i)))
                aktiveSeiten.remove(i--);

        // Aktiviere alle neuen Seiten
        if (aktiviereNeueSeiten)
            for (JCheckBox checkBox : seiteProCheckbox.keySet())
                if (seitenImGraph.indexOf(seiteProCheckbox.get(checkBox)) >= alteGroesse) {
                    checkBox.setSelected(true);
                    aktiveSeiten.add(seiteProCheckbox.get(checkBox));
                }


        this.removeAll();
        this.setLayout(new BorderLayout());
        this.add(panel, BorderLayout.WEST);

        SwingUtilities.invokeLater(() -> {
            revalidate();
            repaint();
        });
    }

    /**
     * Erzeugt das Auswahlpanel fuer eine Seite.
     *
     * @param s        Seite
     * @param markiert gibt an, ob die Checkbox markiert sein soll.
     * @return Panel mit Icon, Farbwahlknop und Auswahlhaken, ob die Seite angezeigt werden soll.
     */
    private JPanel erzeugePanelProSeite(Seite s, boolean markiert) {
        JPanel p = new JPanel(new BorderLayout(5, 5));
        JButton button = s.getFarbButton();
        button.addActionListener(getActionL());

        JCheckBox box = new JCheckBox(s.toString());
        box.setSelected(markiert);
        box.addActionListener(getActionL());

        p.add(box, BorderLayout.WEST);
        p.add(button, BorderLayout.CENTER);
        p.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));

        seiteProCheckbox.put(box, s);

        return p;
    }

    /**
     * @return the actionL
     */
    private ActionListener getActionL() {
        if (actionL == null) {
            actionL = e -> {
                if (e.getSource() instanceof JCheckBox) {
                    JCheckBox source = (JCheckBox) e.getSource();
                    if (source.isSelected()) {
                        aktiveSeiten.add(seiteProCheckbox.get(source));
                        if (oben)
                            gui.getSeitenUnten().remove(seiteProCheckbox.get(source));
                        else
                            gui.getSeitenOben().remove(seiteProCheckbox.get(source));
                    } else
                        aktiveSeiten.remove(seiteProCheckbox.get(source));

                    SwingUtilities.invokeLater(this::repaint);

                    gui.aktualisiereDarstellung();
                }
            };
        }
        return actionL;
    }
    
    public void aktualisiereDarstellung() {
    	for (Map.Entry<JCheckBox,Seite> entry : seiteProCheckbox.entrySet()) {
    		entry.getKey().setSelected(aktiveSeiten.contains(entry.getValue()));
    	}
        SwingUtilities.invokeLater(this::repaint);
    }
}
