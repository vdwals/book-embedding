/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie können es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 *     veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es nützlich sein wird, aber
 *     OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License für weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem;

import uni.wue.boem.aktionen.*;
import uni.wue.boem.daten.Graph;
import uni.wue.boem.daten.Kante;
import uni.wue.boem.daten.Knoten;
import uni.wue.boem.daten.Seite;
import uni.wue.boem.elemente.ColorIcon;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.util.Arrays;
import java.util.LinkedList;

/**
 * Klasse fuer die Darstellung und Bearbeitung der Bucheinbettung. Stellt direkt
 * die Oberflaeche, auf der gezeichnet wird.
 *
 * @author Dennis van der Wals
 */
class GraphGUI extends JPanel {
	/**
	 * Generated SerialID
	 */
	private static final long serialVersionUID = 6880774984712648047L;

	private final GraphGUIManager main;

	/* Kontrollelemente */
	private LinkedList<Seite> seitenOben, seitenUnten;
	private AktionManager aktionManager;

	/* GUI Elemente */
	private SeitenLeiste oben, unten;
	private JPanel buch;
	private JFrame frame;
	private double transFormWidth = -1, transFormHeight = -1;

	/* Datenelemente */
	private Graph graph;

	/* Aktionselemente */
	private int mittlereKoordinate;
	private Knoten knotenInBewegung;
	private Kante nahsteKante;
	private boolean guiBlockiert, nahsteKanteFrei = true;

	/* Steuerungselemente */
	private ComponentListener compList;
	private MouseListener ml;
	private MouseMotionListener mml;
	private ActionListener al;
	private PopupMenuListener popUpListener;

	/**
	 * Standardkonstruktor. Erwartet einen AktionManager als Eingabe. Darueber
	 * werden die Knoepfe zum Rueckgaengig machen und wiederherstellen mit den
	 * entsprechenden Aktionen verknuepft.
	 *
	 * @param main
	 *            GraphGUIManager der die simultane Bearbeitung mehrerer
	 *            GraphGUIs ermoeglicht.
	 * @param aktionManager
	 *            AktionManager zum aufzeichnen und Anwenden von Aktionen, die
	 *            Rueckgaengig gemacht oder wiederhergestellt werden koennen.
	 */
	public GraphGUI(GraphGUIManager main, AktionManager aktionManager) {
		this.main = main;

		this.setLayout(new GridLayout());
		this.setBackground(Color.WHITE);
		this.setBorder(BorderFactory.createLineBorder(Color.BLACK));

		this.aktionManager = aktionManager;

		this.addComponentListener(getCompList());
		this.addMouseListener(getMl());
		this.addMouseMotionListener(getMml());

		this.setDoubleBuffered(true);
	}

	/**
	 * @return the oben
	 */
	private SeitenLeiste getOben() {
		if (oben == null)
			oben = new SeitenLeiste(this, this.getSeitenOben(), true);
		return oben;
	}

	/**
	 * @return the unten
	 */
	private SeitenLeiste getUnten() {
		if (unten == null)
			unten = new SeitenLeiste(this, this.getSeitenUnten(), false);
		return unten;
	}

	@Override
	public void paint(Graphics g2) {
		super.paint(g2);

		Graphics2D g = (Graphics2D) g2;

		// Setzt eine hoehere Qualitaet der Graphik
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

		// Zeichnet den "Buchruecken"
		g.setColor(Color.BLACK);
		g.drawLine(0, mittlereKoordinate, getWidth(), mittlereKoordinate);

		/*
		 * Transformiere die Zeichnung, wenn es sich nicht um die Hauptgui
		 * handelt, für die die Koordinaten berechnet wurden.
		 */
		main.setTransform();
		g.transform(getTransform());

		/*
		 * Prueft, ob ein Graph geladen ist um NullPointer Exceptions zu
		 * vermeiden
		 */
		if (graph != null && getSeitenOben() != null && getSeitenUnten() != null) {
			// Zeichne Kanten
			for (Seite seite : getSeitenOben())
				graph.zeichneSeite(seite, g, true);

			for (Seite seite : getSeitenUnten())
				graph.zeichneSeite(seite, g, false);

			// Zeichne Knoten
			graph.zeichneKnoten(g);
		}
	}

	/**
	 * Berechnet den Skalierungswert. Versucht die Seitenverhaeltnisse
	 * beizubehalten um Verzerrungen zu vermeiden.
	 *
	 * @return AffineTransform instanz mit passender Skalierung, sodass die
	 *         Berechnungen der HauptGUI uebernommen werden koennen.
	 */
	private AffineTransform getTransform() {
		AffineTransform af = new AffineTransform();

		if (transFormHeight == -1 || transFormWidth == -1)
			return af;

		double transFormX = this.getWidth() / transFormWidth;
		double transFormY = this.getHeight() / transFormHeight;

		af.scale(transFormX, transFormY);
		// double transform = Math.min(transFormX, transFormY);
		// af.scale(transform, transform);

		return af;
	}

	/**
	 * Eine Liste von Seiten, die auf der oberen haelfte angezeigt werden
	 * sollen.
	 *
	 * @return the seitenOben
	 */
	public LinkedList<Seite> getSeitenOben() {
		if (seitenOben == null)
			seitenOben = new LinkedList<>();
		return seitenOben;
	}

	/**
	 * Eine Liste von Seiten, die auf der unteren haelfte angezeigt werden
	 * sollen.
	 *
	 * @return the seitenUnten
	 */
	public LinkedList<Seite> getSeitenUnten() {
		if (seitenUnten == null)
			seitenUnten = new LinkedList<>();
		return seitenUnten;
	}

	/**
	 * Setzt den Graphen fuer die Buchdarstellung.
	 *
	 * @param graph
	 *            the graph to set
	 */
	public void setGraph(Graph graph) {
		this.graph = graph;
		this.seitenUnten = null;
		this.seitenOben = null;

		if (graph != null) {
			Seite[] seiten = graph.getSeiten();
			int m = (seiten.length + 1) / 2;
			getSeitenOben().addAll(Arrays.asList(seiten).subList(0, m));
			getSeitenUnten().addAll(Arrays.asList(seiten).subList(m, seiten.length));

			getOben().setGraph(graph, getSeitenOben(), false);
			getUnten().setGraph(graph, getSeitenUnten(), false);
		}
	}

	/**
	 * Fuert nur Neuberechnungen und Zeichnungen der Kanten aus.So koennen neue
	 * Kantenwerte berechnet werden, ohne die Knoten zu beeinflussen.
	 *
	 * @param zeichnen
	 *            Legt fest, ob nach der Berechnung gezeichnet werden soll.
	 */
	public void berechneDarstellungKanten(boolean zeichnen) {
		if (graph == null)
			return;

		// Setze Position der oberen Boegen
		for (Seite seite : getSeitenOben())
			graph.berechneDarstellungBoegen(seite, getHeight(), getWidth());

		// Setzte Position der unteren Boegen
		for (Seite seite : getSeitenUnten())
			graph.berechneDarstellungBoegen(seite, getHeight(), getWidth());

		if (zeichnen)
			SwingUtilities.invokeLater(this::repaint);
	}

	/**
	 * Ersetzt den AktionManager.
	 *
	 * @param aktionManager
	 *            the aktionManager to set
	 */
	public void setAktionManager(AktionManager aktionManager) {
		this.aktionManager = aktionManager;
	}

	/**
	 * Erzeugt den ComponentListener und gibt ihn zurueck. Loest eine
	 * Neuzeichnen und berechnen aus, wenn die Groesse des Fensters sich
	 * aendert.
	 *
	 * @return the compList ComponentenListener, der auf eine
	 *         Groessenveraenderung des Fensters reagiert.
	 */
	private ComponentListener getCompList() {
		if (compList == null)
			compList = new ComponentListener() {

				@Override
				public void componentHidden(ComponentEvent e) {
				}

				@Override
				public void componentMoved(ComponentEvent e) {
				}

				@Override
				public void componentResized(ComponentEvent e) {
					mittlereKoordinate = getHeight() / 2;

					if (graph != null)
						main.aktualisiereDarstellung();
				}

				@Override
				public void componentShown(ComponentEvent e) {
				}
			};
		return compList;
	}

	/**
	 * Erzeugt einen MouseListener und gibt ihn zurueck. Dieser regrisitriert
	 * Mausaktionen des Benutzers und steuert entsprechendes Verhalten.
	 *
	 * @return the ml MouseListener, der Benutzereingaben verarbeitet.
	 */
	private MouseListener getMl() {
		if (ml == null)
			ml = new MouseListener() {

				@Override
				public void mouseReleased(MouseEvent e) {
					legeKnotenAb();
				}

				@Override
				public void mousePressed(MouseEvent e) {
					if (!guiBlockiert) {
						AffineTransform af = getTransform();
						int x = (int) (e.getX() / af.getScaleX() - af.getTranslateX());
						int y = (int) (e.getY() / af.getScaleY() - af.getTranslateY());

						nehmeKnotenAuf(x, y);
					}
				}

				@Override
				public void mouseExited(MouseEvent e) {
				}

				@Override
				public void mouseEntered(MouseEvent e) {
				}

				@Override
				public void mouseClicked(MouseEvent e) {
					if (graph != null && !guiBlockiert) {
						AffineTransform af = getTransform();
						int x = (int) (e.getX() / af.getScaleX() - af.getTranslateX());
						int y = (int) (e.getY() / af.getScaleY() - af.getTranslateY());

						if (SwingUtilities.isLeftMouseButton(e))
							verschiebeNahsteKante(x, y);

						else if (CommonSettings.DEVELOPER && SwingUtilities.isRightMouseButton(e))
							bearbeiteGraph(x, y);
					}
				}

				/**
				 * Verschiebt die nahste Kante zur Auswahl auf eine andere
				 * Seite. Sind nur zwei Seiten vorhanden, wird die jeweils
				 * andere Seite ausgewaehlt, ansonsten ein Popup menue
				 * aufgerufen.
				 *
				 * @param x
				 *            x-Koordinate an der das Popup Menue erscheint
				 * @param y
				 *            y-Koordinate an der das Popup Menue erscheint
				 */
				private void verschiebeNahsteKante(int x, int y) {
					if (GraphGUI.this.nahsteKante == null)
						return;

					aktionManager.starteNeueAktion(new BewegeKanteAktion(graph, GraphGUI.this.nahsteKante));

					if (graph.anzahlSeiten() > 2)
						erzeugeMenu(GraphGUI.this.nahsteKante).show(GraphGUI.this, x, y);

					else if (graph.anzahlSeiten() == 2) {
						if (graph.getIndexDerSeite(graph.getSeiteMitKante(GraphGUI.this.nahsteKante)) == 0)
							verschiebeKante(1);
						else
							verschiebeKante(0);
					}
				}

				/**
				 * Verschiebt eine Kante auf eine Zielseite. Die Zielseite kann
				 * durch eine Zahl angegeben werden, da sie sortiert in einem
				 * Array vorliegen und damit immer eindeutig zuordbar sind.
				 * Aktuallisiert anschliessend die Darstellung.
				 *
				 * @param zielSeite
				 *            Index der Zielseite im Array = Nummer der
				 *            Zielseite
				 */
				private void verschiebeKante(int zielSeite) {
					if (GraphGUI.this.nahsteKante == null)
						return;

					graph.kanteVerschieben(GraphGUI.this.nahsteKante, zielSeite);
					aktionManager.beendeAktiveAktion();

					main.aktualisiereDarstellung();
				}

				/**
				 * Erzeugt ein Kontektmenue zum Bearbeiten der Seite
				 *
				 * @param x
				 *            x-Koordinate fuer Kontextmenue
				 * @param y
				 *            y-Koordinate fuer Kontextmenue
				 */
				private void bearbeiteGraph(int x, int y) {
					erzeugeKontextMenu(x, y).show(GraphGUI.this, x, y);
				}

				/**
				 * Prueft, ob die Koordinaten in auf einem Knoten liegen, und
				 * initialisiert dessen Bewegung. Dazu wird eine Bewegungsaktion
				 * gestartet, sodass die Bewegung rueckgaengig gemacht werden
				 * kann und der Knoten wird als beweglicher Knoten markiert. Er
				 * folgt den Eingaben der Funktion bewegeKnoten().
				 *
				 * @param x
				 *            x-Koordinate (des Mauszeigers z.B.)
				 * @param y
				 *            y-Koordinate (des Mauszeigers z.B.)
				 */
				private void nehmeKnotenAuf(int x, int y) {
					if (graph == null)
						return;

					// Pruefe, ob ein Knoten angeklickt wurde
					knotenInBewegung = graph.getGeklicktenKnoten(x, y);

					if (knotenInBewegung != null && aktionManager != null)
						aktionManager.starteNeueAktion(new BewegungeKnotenAktion(graph, knotenInBewegung));

				}

				/**
				 * Legt einen Knoten, der in Bewegung ist, an der letzten
				 * Position zu der er bewegt wurde mit bewegeKnoten(int x, int
				 * y) ab.
				 */
				private void legeKnotenAb() {
					if (knotenInBewegung == null)
						return;

					knotenInBewegung = null;

					if (aktionManager != null)
						aktionManager.beendeAktiveAktion();

					graph.knotenSortieren();
					main.aktualisiereDarstellung();
				}

				/**
				 * Erzeugt ein Popup Menue mit allen Eintraegen, die fuer die
				 * nahste Kante relevant sind. Es werden alle Seiten
				 * ausgewaehlt, bis auf die, auf der die Kante gerade liegt. Bei
				 * Auswahl einer Seite, wird die Kante auf diese Seite
				 * verschoben.
				 *
				 * @param k
				 *            Kante, fuer die das Menue erzeugt wird.
				 * @return Gibt das Menue zurueck, in dem ausgewaehlet werden
				 *         kann, auf welche Seite die Kante verschoben werden
				 *         soll.
				 */
				private JPopupMenu erzeugeMenu(Kante k) {
					JPopupMenu menu = new JPopupMenu("Move edge to:");
					menu.addPopupMenuListener(GraphGUI.this.getPopUpListener());

					menu.add(new JLabel("move to page: "));
					menu.addSeparator();

					// Fuege jede Seite hinzu
					for (Seite seite : graph.seitenDieKanteNichtEnthalen(k)) {
						JMenuItem item = new JMenuItem(seite.toString(), new ColorIcon(seite.getFarbe()));
						item.addActionListener(getAl());
						item.setActionCommand(seite.toString());

						menu.add(item);
					}

					return menu;
				}

				/**
				 * Erzeugt einen ActionListener und gibt ihn zurueck. Dieser
				 * verwaltet die Aktion beim benutzen des Popup Menues und loest
				 * die Verschiebung einer Kante von einer Seite auf eine andere
				 * aus.
				 *
				 * @return the al ActionListener
				 */
				private ActionListener getAl() {
					if (al == null)
						al = e -> verschiebeKante(Integer.parseInt(e.getActionCommand()));

					return al;
				}

				/**
				 * Erzeugt ein Kontextmenue zum editieren des Graphen.
				 *
				 * @param x
				 *            x-Koordiante
				 * @param y
				 *            y-Koordinate
				 * @return Kontextmenug mit Bearbeitungsoptionen.
				 */
				private JPopupMenu erzeugeKontextMenu(int x, int y) {
					JPopupMenu menu = new JPopupMenu("Edit graph:");
					menu.addPopupMenuListener(GraphGUI.this.getPopUpListener());

					// Funktionen hinzufuegen
					JMenuItem itemAddKnoten = new JMenuItem("Add node");
					itemAddKnoten.addActionListener(e -> {
						Knoten neuerKnoten = graph.knotenErzeugen();
						if (neuerKnoten != null) {
							aktionManager.starteNeueAktion(new BearbeiteKnotenAktion(graph, neuerKnoten, true));
							aktionManager.beendeAktiveAktion();
							main.aktualisiereDarstellung();
						}
					});
					menu.add(itemAddKnoten);

					Knoten k = graph.getGeklicktenKnoten(x, y);
					if (k != null) {
						/*
						 * Biete nur an einen Knoten zu loeschen, wenn einer
						 * angeklickt wurde
						 */
						JMenuItem itemDel = new JMenuItem("Remove node");
						itemDel.addActionListener(e -> {
							aktionManager.starteNeueAktion(new BearbeiteKnotenAktion(graph, k, false));
							aktionManager.beendeAktiveAktion();
							main.aktualisiereDarstellung();
						});
						menu.add(itemDel);
					}

					menu.addSeparator();

					JMenuItem itemAddPage = new JMenuItem("Add page");
					itemAddPage.addActionListener(e -> {
						Seite neueSeite = graph.seiteErzeugen(true);
						if (neueSeite != null) {
							aktionManager.starteNeueAktion(new BearbeiteSeitenAktion(graph, neueSeite));
							aktualisiereLeisten();
							aktionManager.beendeAktiveAktion();
						}
					});
					menu.add(itemAddPage);

					if (graph.anzahlSeiten() > 1) {
						/*
						 * Biete nur an eine Seite zu loeschen, wenn mehr als
						 * eine vorhanden ist
						 */
						JMenuItem itemDel = new JMenuItem("Remove page");
						itemDel.addActionListener(e -> {
							Seite[] seitenAuswahl = graph.seitenAuswahl();
							if (seitenAuswahl[0] != null) {
								aktionManager.starteNeueAktion(new BearbeiteSeitenAktion(graph, seitenAuswahl));
								aktualisiereLeisten();
								aktionManager.beendeAktiveAktion();
								main.aktualisiereDarstellung();
							}
						});
						menu.add(itemDel);
					}

					menu.addSeparator();

					if (graph.anzahlKnoten() > 1 && graph.anzahlSeiten() > 0) {
						/*
						 * Biete nur an Kanten zu bearbeiten, wenn wenigstens 2
						 * Knoten und eine Seite existieren
						 */
						JMenuItem itemAddKante = new JMenuItem("Add edge");
						itemAddKante.addActionListener(e -> {
							Kante neueKante = graph.kanteErzeugen();
							if (neueKante != null) {
								aktionManager.starteNeueAktion(new BearbeiteKanteAktion(graph, neueKante, true));
								aktionManager.beendeAktiveAktion();
								main.berechneDarstellungKanten(true);
							}
						});
						menu.add(itemAddKante);

						if (GraphGUI.this.nahsteKante != null) {
							/*
							 * Biete nur an eine Kante zu loeschen, wenn eine
							 * angeglickt wurde
							 */
							JMenuItem itemDel = new JMenuItem("Remove edge");
							itemDel.addActionListener(e -> {
								aktionManager.starteNeueAktion(
										new BearbeiteKanteAktion(graph, GraphGUI.this.nahsteKante, false));
								aktionManager.beendeAktiveAktion();
								main.berechneDarstellungKanten(true);
							});
							menu.add(itemDel);
						}
					}

					return menu;
				}
			};
		return ml;
	}

	/**
	 * Erzeugt einen MouseMotionListener und gibt ihn zurueck. Wird verwendet um
	 * Knoten zu bewegen und Kanten hervorzuheben.
	 *
	 * @return the mml MouseMotionListener
	 */
	private MouseMotionListener getMml() {
		if (mml == null)
			mml = new MouseMotionListener() {

				@Override
				public void mouseMoved(MouseEvent e) {
					if (!guiBlockiert) {
						AffineTransform af = getTransform();
						int x = (int) (e.getX() / af.getScaleX() - af.getTranslateX());
						int y = (int) (e.getY() / af.getScaleY() - af.getTranslateY());

						if (GraphGUI.this.nahsteKanteFrei)
							bestimmeNahsteKante(x, y);
					}
				}

				@Override
				public void mouseDragged(MouseEvent e) {
					if (!guiBlockiert) {
						AffineTransform af = getTransform();
						int x = (int) (e.getX() / af.getScaleX() - af.getTranslateX());

						bewegeKnoten(x);
					}
				}

				/**
				 * Bewegt einen Knoten zur angegebenen x-koordinate entlang der
				 * mittleren Achse. Dabei wird geprueft, ob die Reihenfolge der
				 * Darstellung der Reihenfolge der Knoten entspricht.
				 *
				 * @param x
				 *            x-Koordinate (des Mauszeigers z.B.)
				 */
				private void bewegeKnoten(int x) {
					if (knotenInBewegung == null)
						return;

					graph.bewegeKnoten(knotenInBewegung, x, mittlereKoordinate);

					/*
					 * Hierdurch werden die Knoten wieder gleichmaeßig auf der
					 * mittleren Achse verteilt entsprechend ihrer durch die
					 * Bewegung veraenderten Reichenfolge.
					 */
					graph.knotenSortieren();
					main.berechneDarstellungKnoten(true);

					/*
					 * Der bewegte Knoten muss nach der gleichmaeßigen
					 * Verteilung wieder zum Mauszeiger verschoben werden.
					 */
					graph.bewegeKnoten(knotenInBewegung, x, mittlereKoordinate);
					main.berechneDarstellungKanten(true);
				}

				/**
				 * Bestimmt, ob die Koordinaten nahe einer Kante sind und hebt
				 * die nahste Kante hervor.
				 *
				 * @param x
				 *            x-Koordinate (des Mauszeigers z.B.)
				 * @param y
				 *            y-Koordinate (des Mauszeigers z.B.)
				 */
				private void bestimmeNahsteKante(int x, int y) {
					if (graph == null)
						return;

					double kleinsteDistanz = Double.MAX_VALUE;
					Kante nahsteKante = null;
					for (Seite seite : getSeitenOben()) {
						for (Kante kante : seite.getKanten()) {
							double d = Math.abs(kante.getArc().abstandZumRand(x, y, true) - 1);
							if (d < CommonSettings.MINIMUM_DISTANCE_TO_EDGE && d < kleinsteDistanz) {
								kleinsteDistanz = d;
								nahsteKante = kante;
							}
						}
					}
					for (Seite seite : getSeitenUnten()) {
						for (Kante kante : seite.getKanten()) {
							double d = Math.abs(kante.getArc().abstandZumRand(x, y, false) - 1);
							if (d < CommonSettings.MINIMUM_DISTANCE_TO_EDGE && d < kleinsteDistanz) {
								kleinsteDistanz = d;
								nahsteKante = kante;
							}
						}
					}

					/*
					 * Wenn eine andere Kante zuvor hervorgehoben wurde, wird
					 * diese nun nicht mehr hervorgehoben.
					 */
					if (GraphGUI.this.nahsteKante != null)
						graph.kanteHervorheben(GraphGUI.this.nahsteKante, false);

					GraphGUI.this.nahsteKante = nahsteKante;

					/*
					 * Auf Grund des maximalen Abstandes, kann es sein, dass es
					 * keine nahste Kante gibt.
					 */
					if (nahsteKante != null)
						// Die nahste Kante wird hervorgehoben
						graph.kanteHervorheben(nahsteKante, true);

					main.aktualisiereDarstellung();
				}
			};
		return mml;
	}

	/**
	 * Aktualisiert die Darstellung der Seitenleisten.
	 */
	public void aktualisiereLeisten() {
		oben.setGraph(graph, seitenOben, false);
		unten.setGraph(graph, seitenUnten, true);
	}

	/**
	 * Gibt die Oberflaeche zusammen mit den Verziehrungen wie den Seitenleisten
	 * und Rahmen zurueck.
	 *
	 * @return Buch
	 */
	public JPanel getBuch() {
		if (buch == null) {
			buch = new JPanel(new BorderLayout(5, 5));
			buch.add(this, BorderLayout.CENTER);

			JPanel oben = new JPanel(new BorderLayout(5, 5));
			oben.add(new JLabel("  active pages:"), BorderLayout.WEST);
			oben.add(new JScrollPane(getOben(), JScrollPane.VERTICAL_SCROLLBAR_NEVER,
					JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS), BorderLayout.CENTER);

			JPanel unten = new JPanel(new BorderLayout(5, 5));
			unten.add(new JLabel("  active pages:"), BorderLayout.WEST);
			unten.add(new JScrollPane(getUnten(), JScrollPane.VERTICAL_SCROLLBAR_NEVER,
					JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS), BorderLayout.CENTER);

			buch.add(oben, BorderLayout.NORTH);
			buch.add(unten, BorderLayout.SOUTH);
		}
		return buch;
	}

	public JFrame getFrame() {
		if (frame == null) {
			frame = new JFrame();
			frame.setLayout(new GridLayout(1, 1));
			frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			frame.addWindowListener(new WindowListener() {
				@Override
				public void windowOpened(WindowEvent e) {

				}

				@Override
				public void windowClosing(WindowEvent e) {
					GraphGUI.this.main.getGraphGUIs().remove(GraphGUI.this);
				}

				@Override
				public void windowClosed(WindowEvent e) {

				}

				@Override
				public void windowIconified(WindowEvent e) {

				}

				@Override
				public void windowDeiconified(WindowEvent e) {

				}

				@Override
				public void windowActivated(WindowEvent e) {

				}

				@Override
				public void windowDeactivated(WindowEvent e) {

				}
			});
			frame.add(this.getBuch());
			frame.setSize(CommonSettings.WINDOW_SIZE_START);
		}
		return frame;
	}

	/**
	 * Aktualisiert die Darstellung.
	 */
	public void aktualisiereDarstellung() {
		main.aktualisiereDarstellung();
		oben.aktualisiereDarstellung();
		unten.aktualisiereDarstellung();
	}

	/**
	 * Blockiert die GUI oder gibt sie frei.
	 *
	 * @param guiBlockiert
	 *            Legt fest, ob die GUI blockiert werden soll.
	 */
	void setGuiBlockiert(boolean guiBlockiert) {
		this.guiBlockiert = guiBlockiert;
	}

	/**
	 * Fuegt Informationen zum Tranformieren der Darstellung hinzu.
	 *
	 * @param witdth
	 *            Breite der Darstellungen, fuer die Koordinaten berechnet
	 *            wurden.
	 * @param height
	 *            Hoehe der Darstellung, fur die Koordinaten berechnet wurden.
	 */
	void setTransform(double witdth, double height) {
		transFormWidth = witdth;
		transFormHeight = height;
	}

	private PopupMenuListener getPopUpListener() {
		if (popUpListener == null)
			popUpListener = new PopupMenuListener() {
				@Override
				public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
					GraphGUI.this.nahsteKanteFrei = false;
				}

				@Override
				public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
					GraphGUI.this.nahsteKanteFrei = true;
				}

				@Override
				public void popupMenuCanceled(PopupMenuEvent e) {
					GraphGUI.this.nahsteKanteFrei = true;
				}
			};
		return popUpListener;
	}
}
