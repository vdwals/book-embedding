/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie k�nnen es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder sp�teren
 *     ver�ffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es n�tzlich sein wird, aber
 *     OHNE JEDE GEW�HRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gew�hrleistung der MARKTF�HIGKEIT oder EIGNUNG F�R EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License f�r weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem.aktionen;

import uni.wue.boem.daten.Graph;
import uni.wue.boem.daten.Kante;
import uni.wue.boem.daten.Seite;

/**
 * Klasse fuer die Aktion eine Kante auf eine andere Seite zu verschieben. Ungueltige Eingaben fuehren dazu, dass die Aktion keine Unterschiede erzeugt und damit nicht dem Aktionenstapel hinzugefuegt wird.
 *
 * @author Dennis van der Wals
 */
public class BewegeKanteAktion extends Aktion {
    private final Kante kante;
    private Seite startSeite, endSeite;

    public BewegeKanteAktion(Graph g, Kante k) {
        super(g);
        this.kante = k;
    }

    @Override
    public void rueckgaengig() {
        graph.kanteVerschieben(kante, startSeite);
    }

    @Override
    public void wiederherstellen() {
        graph.kanteVerschieben(kante, endSeite);
    }

    @Override
    public void starten() {
        if (graph == null)
            return;

        startSeite = graph.getSeiteMitKante(kante);
    }

    @Override
    public void beenden() {
        if (graph == null)
            return;

        endSeite = graph.getSeiteMitKante(kante);
    }

    @Override
    public boolean gibtEsUnterschiede() {
        return !(startSeite == null || endSeite == null) && startSeite != endSeite;
    }

    public String toString() {
        return "Moved edge " + kante + " from " + startSeite + " to " + endSeite;
    }
}
