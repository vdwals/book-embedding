/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie können es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 *     veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es nützlich sein wird, aber
 *     OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License für weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem.aktionen;

import uni.wue.boem.daten.Graph;

/**
 * Abtrakte Klasse fuer alle Aktionen, die Rueckgaengig gemacht oder wiederholt werden koennen.
 *
 * @author Dennis van der Wals
 */
public abstract class Aktion {
    final Graph graph;

    /**
     * Konstruktor.
     *
     * @param g Graph, auf dem die Veraenderungen angewendet werden.
     */
    Aktion(Graph g) {
        this.graph = g;
    }

    /**
     * Macht die entsprechende Aktion rueckgaengig
     */
    abstract void rueckgaengig();

    /**
     * Macht das rueckgaengig machen der Aktion rueckgaengig
     */
    abstract void wiederherstellen();

    /**
     * Startet die Aktion. Hier werden alle zum Rueckgaengig machen notwendigen Informationen gesammelt und die Aktion anschließend durchgefuehrt.
     */
    abstract void starten();

    /**
     * Beendet die Aktion. Hier werden alle zum Rueckgaengig machen oder Wiederherstellen notwendigen Informationen gesammelt.
     */
    abstract void beenden();

    /**
     * Manche Aktionen, die gestartet wurden, haben nach der Beendigung den Graphen nicht veraendert. Diese Methode gibt zurueck, ob die Aktion eine Veraenderung vorgenommen hat.
     *
     * @return Wahrheitswert ueber die Veraenderung des Graphen durch die Aktion
     */
    abstract boolean gibtEsUnterschiede();
}
