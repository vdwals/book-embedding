/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie k�nnen es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder sp�teren
 *     ver�ffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es n�tzlich sein wird, aber
 *     OHNE JEDE GEW�HRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gew�hrleistung der MARKTF�HIGKEIT oder EIGNUNG F�R EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License f�r weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem.aktionen;

import uni.wue.boem.daten.Graph;
import uni.wue.boem.daten.Knoten;

/**
 * Klasse fuer die Aktion der Bewegung eines Knotens.
 *
 * @author Dennis van der Wals
 */
public class BewegungeKnotenAktion extends Aktion {
    private final Knoten knotenBewegt;
    private int startPosition, endPosition;

    /**
     * Konstruktor. Ungueltige Eingaben fuehren dazu, dass die Aktion keine Unterschiede erzeugt und damit nicht dem Aktionenstapel hinzugefuegt wird.
     *
     * @param g                Graph
     * @param knotenInBewegung Knoten, der bewegt wird.
     */
    public BewegungeKnotenAktion(Graph g, Knoten knotenInBewegung) {
        super(g);
        knotenBewegt = knotenInBewegung;
    }

    @Override
    public void rueckgaengig() {
        graph.knotenVerschieben(knotenBewegt, startPosition, false);
    }

    @Override
    public void wiederherstellen() {
        graph.knotenVerschieben(knotenBewegt, endPosition, false);
    }

    @Override
    public void starten() {
        if (graph == null || knotenBewegt == null)
            return;

        /* Sichere Startposition des Knotens */
        startPosition = graph.getIndexDesKnoten(knotenBewegt);
        if (startPosition == -1)
            startPosition = 0;
    }

    @Override
    public void beenden() {
        if (graph == null || knotenBewegt == null)
            return;

        /* Sichere Endposition des Knotens */
        endPosition = graph.getIndexDesKnoten(knotenBewegt);
        if (endPosition == -1)
            endPosition = 0;
    }

    @Override
    public boolean gibtEsUnterschiede() {
        return knotenBewegt != null && startPosition != endPosition;
    }

    public String toString() {
        return "Moved node " + knotenBewegt + " from " + startPosition + " to " + endPosition;
    }
}
