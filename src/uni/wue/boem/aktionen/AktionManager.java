/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie k�nnen es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder sp�teren
 *     ver�ffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es n�tzlich sein wird, aber
 *     OHNE JEDE GEW�HRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gew�hrleistung der MARKTF�HIGKEIT oder EIGNUNG F�R EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License f�r weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem.aktionen;

import javax.swing.*;
import java.util.LinkedList;

/**
 * Klasse zum Verwalten von Aktionen, die rueckgaengig gemacht oder
 * wiederhergestellt werden koennen.
 *
 * @author Dennis van der Wals
 */
public class AktionManager {
    private final LinkedList<Aktion> aktionenRueckgaengig;
    private final LinkedList<Aktion> aktionenWiederherstellen;
    private final JButton undo, redo;
    private Aktion aktiveAktion;

    /**
     * Standardkonstruktor. Initialisiert die Listen.
     *
     * @param undo JButton fuer das Rueckgaengigmachen. Wenn <code>null</code>, wird ein neuer Button erzeugt.
     * @param redo JButton fuer die Wiederherstellung. Wenn <code>null</code>, wird ein neuer Button erzeugt.
     */
    public AktionManager(JButton undo, JButton redo) {
        aktionenRueckgaengig = new LinkedList<>();
        aktionenWiederherstellen = new LinkedList<>();

        if (undo != null)
            this.undo = undo;
        else
            this.undo = new JButton();

        if (redo != null)
            this.redo = redo;
        else
            this.redo = new JButton();

        aktualisiereSchalter(false, false);
    }

    /**
     * Erzeugt eine Liste von Aktionen als Tooltip fuer einen Button
     *
     * @param aktionen Liste mit Aktionen
     * @return String mit gelisteten Aktionen
     */
    private static String generateTooltip(Iterable<Aktion> aktionen) {
        StringBuilder s = new StringBuilder();
        s.append("<html>");
        for (Aktion a : aktionen) {
            s.append(a);
            s.append("<br>");
        }
        s.delete(s.lastIndexOf("<"), s.lastIndexOf(">") + 1);
        s.append("</html>");

        return s.toString();
    }

    /**
     * Fuegt eine neue Aktion der Liste rueckgaengigmachbarer Aktionen hinzu.
     * Loechst dabei die Wiederherstellungsliste.
     *
     * @param neu neue Aktion. Wenn <code>null</code> wird nichts durchgefuehrt.
     */
    private void neueAktion(Aktion neu) {
        if (neu == null)
            return;

        aktionenRueckgaengig.push(neu);
        aktionenWiederherstellen.clear();

        aktualisiereSchalter(false, true);
    }

    /**
     * Nimmt eine neue Aktion auf, speichert sie als aktiveAktion und startet
     * sie. Beim starten, wird der relevante Ist-Zustand erfasst, sodass die
     * Aktion rueckgaengig gemacht werden kann. Aktionen, die nur einen Schritt umfassen, werden dadurch durchgefuehrt (z.B. Hinzufuegen/Loeschen einer Kante). Ist noch eine andere Aktion aktiv, wird diese beendet.
     *
     * @param neu Neue Aktion. Wenn <code>null</code> wird nur die noch laufende aktive Aktion beendet.
     */
    public void starteNeueAktion(Aktion neu) {
        if (aktiveAktion != null)
            beendeAktiveAktion();

        aktiveAktion = neu;

        if (neu != null)
            aktiveAktion.starten();
    }

    /**
     * Beendet die letzte, aktive Aktion. Dabei wird der neue Ist-Zustand nach
     * der Aktion erfasst, sodass nach dem rueckgaengig machen, die Aktion
     * wiederhergestellt werden kann. Dabei wird die Aktion nur als rueckgaengig
     * machbar erfasst, wenn sich die Zustaende unterscheiden.
     */
    public void beendeAktiveAktion() {
        if (aktiveAktion == null)
            return;

        aktiveAktion.beenden();

        if (aktiveAktion.gibtEsUnterschiede())
            neueAktion(aktiveAktion);

        aktiveAktion = null;
    }

    /**
     * Gibt zurueck, ob es rueckgaengig machbare Aktionen gibt.
     *
     * @return Wahrheitswert
     */
    private boolean kannRueckgaengig() {
        return !aktionenRueckgaengig.isEmpty();
    }

    /**
     * Gibt zurueck, ob es wiederherstellbare Aktionen gibt.
     *
     * @return Wahrheitswert
     */
    private boolean kannWiederherstellen() {
        return !aktionenWiederherstellen.isEmpty();
    }

    /**
     * Macht die letzte Aktion rueckgaengig und macht sie wiederherstellbar.
     *
     * @return gibt die rueckgaengig gemachte Aktion zurueck.
     */
    public Aktion rueckgaengig() {
        if (!kannRueckgaengig())
            return null;

        Aktion a = aktionenRueckgaengig.poll();
        a.rueckgaengig();
        aktionenWiederherstellen.push(a);

        aktualisiereSchalter(true, kannRueckgaengig());

        return a;
    }

    /**
     * Stellt die letzte, rueckgaengig gemachte Aktion wieder her.
     *
     * @return Gibt die wiederhergestellte Aktion zurueck.
     */
    public Aktion wiederherstellen() {
        if (!kannWiederherstellen())
            return null;

        Aktion a = aktionenWiederherstellen.poll();
        a.wiederherstellen();
        aktionenRueckgaengig.push(a);

        aktualisiereSchalter(kannWiederherstellen(), true);

        return a;
    }

    /**
     * Aktualisiert die Schalter zum rueckgaengig machen und wiederherstellen und deren Tooltips
     *
     * @param redo Einstellung fuer den Wiederherstellungsschalter
     * @param undo Einstellung fuer den Ruckgaengigschalter
     */
    private void aktualisiereSchalter(boolean redo, boolean undo) {
        this.redo.setEnabled(redo);
        this.undo.setEnabled(undo);

        if (redo)
            this.redo.setToolTipText(generateTooltip(aktionenWiederherstellen));
        else
            this.redo.setToolTipText(null);

        if (undo)
            this.undo.setToolTipText(generateTooltip(aktionenRueckgaengig));
        else
            this.undo.setToolTipText(null);
    }

}
