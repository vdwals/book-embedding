/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie können es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 *     veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es nützlich sein wird, aber
 *     OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License für weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem.aktionen;

import uni.wue.boem.daten.Graph;
import uni.wue.boem.daten.Kante;
import uni.wue.boem.daten.Knoten;

import java.util.LinkedList;

/**
 * Klasse fuer Aktionen zum erstellen und Loeschen von Knoten.
 *
 * @author Dennis van der Wals
 */
public class BearbeiteKnotenAktion extends Aktion {
    private final boolean erzeugt;
    private final Knoten knoten;
    private final LinkedList<Kante> kanten;

    /**
     * Konstruktor. Unterscheidet zwischen erzeugtem und zerst�rtem Knoten. Ungueltige Eingaben fuehren dazu, dass die Aktion keine Unterschiede erzeugt und damit nicht dem Aktionenstapel hinzugefuegt wird.
     *
     * @param g       Graph
     * @param k       betroffener Knoten
     * @param erzeugt Bestimmt, ob dieser bei der Aktion erzeugt oder zerst�rt wurde
     */
    public BearbeiteKnotenAktion(Graph g, Knoten k, boolean erzeugt) {
        super(g);
        this.knoten = k;
        this.erzeugt = erzeugt;
        this.kanten = new LinkedList<>();
    }

    @Override
    public void rueckgaengig() {
        if (erzeugt)
            /* Wurde der Knoten erzeugt, loesche ihn einfach */
            graph.knotenLoeschen(knoten);

        else {
            /* Wurde ein Knoten geloescht, stelle ihn und seine Kanten wieder her */
            graph.knotenHinzufuegen(knoten);
            kanten.forEach(graph::kanteHinzufuegen);
        }
    }

    @Override
    public void wiederherstellen() {
        if (erzeugt)
            graph.knotenHinzufuegen(knoten);
        else
            graph.knotenLoeschen(knoten);

    }

    @Override
    public void starten() {
        if (graph == null)
            return;

        if (erzeugt)
            /* Wurde der Knoten erzeugt, fuege ihn ein */
            graph.knotenHinzufuegen(knoten);

        else
            /* Wurde der Knoten geloecht, sichere alle dessen Kanten zur wiederherstellung */
            kanten.addAll(graph.knotenLoeschen(knoten));
    }

    @Override
    public void beenden() {
    }

    @Override
    public boolean gibtEsUnterschiede() {
        return graph != null && knoten != null;
    }

    public String toString() {
        return (erzeugt ? "Created: " : "Deleted: ") + "node " + knoten;
    }
}
