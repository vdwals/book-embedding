/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie können es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 *     veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es nützlich sein wird, aber
 *     OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License für weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem.aktionen;

import uni.wue.boem.daten.Graph;
import uni.wue.boem.daten.Kante;
import uni.wue.boem.daten.Seite;

import java.util.LinkedList;

/**
 * Klasse fuer Aktionen zum erstellen und Loeschen von Seiten.
 *
 * @author Dennis van der Wals
 */
public class BearbeiteSeitenAktion extends Aktion {
    private final Seite seiteBearbeitet;
    private final boolean erzeugt;
    private final LinkedList<Kante> kanten;
    private Seite seiteZielFuerKanten;
    private int index;

    /**
     * Konstruktor. Erzeugt eine neue Aktion zum erstellen einer Seite. Ungueltige Eingaben fuehren dazu, dass die Aktion keine Unterschiede erzeugt und damit nicht dem Aktionenstapel hinzugefuegt wird.
     *
     * @param g Graph
     * @param s Erstellte Seite
     */
    public BearbeiteSeitenAktion(Graph g, Seite s) {
        super(g);
        this.seiteBearbeitet = s;
        this.erzeugt = true;
        kanten = new LinkedList<>();
    }

    /**
     * Konstruktur. Erzeugt eine neue Aktion zum entfernen einer Seite. Ungueltige Eingaben fuehren dazu, dass die Aktion keine Unterschiede erzeugt und damit nicht dem Aktionenstapel hinzugefuegt wird.
     *
     * @param g      Graph
     * @param seiten Array aus zwei Seiten. Die zweite Seite bestimmt, ob und auf welche Seite die Kanten verschoben werden sollen, die auf der zu loeschenden Seite liegen. Ist die zweite Seite <code>null</code>, werden die Kanten dereferenziert.
     */
    public BearbeiteSeitenAktion(Graph g, Seite[] seiten) {
        super(g);
        this.seiteBearbeitet = seiten[0];
        this.seiteZielFuerKanten = seiten[1];
        this.erzeugt = false;
        kanten = new LinkedList<>();
    }

    @Override
    public void rueckgaengig() {
        if (erzeugt)
            /* Wurde eine neue Seite erzeugt, kann sei einfach gel�scht werden, da sie keine Kanten enthaelt. */
            graph.seiteLoeschen(seiteBearbeitet, null);

        else {
            /* Wurde eine Seite mit Kanten geloescht, muessen die Kanten wieder auf die Seite verschoben werden */
            graph.seiteHinzufuegen(seiteBearbeitet, index);
            kanten.forEach(kante -> graph.kanteVerschieben(kante, seiteBearbeitet));
        }
    }

    @Override
    public void wiederherstellen() {
        if (erzeugt)
            graph.seiteHinzufuegen(seiteBearbeitet, index);
        else
            graph.seiteLoeschen(seiteBearbeitet, seiteZielFuerKanten);
    }

    @Override
    public void starten() {
        if (graph == null)
            return;

        /* Bestimme Index der Seite. Neue Seiten werden immer am Ende eingefuegt. */
        index = graph.getIndexDerSeite(seiteBearbeitet);
        if (index == -1)
            index = graph.getSeiten().length;

        if (erzeugt)
            /* Fuege neu erzeugte Seite hinzu */
            graph.seiteHinzufuegen(seiteBearbeitet, index);

        else
             /* Fuehre Loeschaktion durch und merke, welche Kanten verschoben - oder geloescht - wurden. Dabei werden
             die Kanten nicht wirklich geloescht, sondern nur dereferenziert. */
            kanten.addAll(graph.seiteLoeschen(seiteBearbeitet, seiteZielFuerKanten));
    }

    @Override
    public void beenden() {

    }

    @Override
    public boolean gibtEsUnterschiede() {
        return graph != null && seiteBearbeitet != null;
    }

    public String toString() {
        return (erzeugt ? "Created: " : "Deleted: ") + "page " + seiteBearbeitet + ((seiteZielFuerKanten == null) ? "" : " and moved edges to " + seiteZielFuerKanten);
    }
}
