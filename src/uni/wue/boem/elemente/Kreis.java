/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie k�nnen es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder sp�teren
 *     ver�ffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es n�tzlich sein wird, aber
 *     OHNE JEDE GEW�HRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gew�hrleistung der MARKTF�HIGKEIT oder EIGNUNG F�R EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License f�r weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem.elemente;

import uni.wue.boem.daten.Knoten;

import java.awt.*;

/**
 * Die Klasse repraesentiert die Darstellung eines Knotens und beinhaltet alle Methoden zur Berechnung der Darstellung.
 *
 * @author Dennis van der Wals
 */
public class Kreis {
    private final Point mittelpunkt, startPunkt;
    private final Knoten knoten;
    private int radius;

    /**
     * Konstruktor.
     *
     * @param knoten Knoten, der durch den Kreis dargestellt wird.
     */
    public Kreis(Knoten knoten) {
        mittelpunkt = new Point();
        startPunkt = new Point();

        this.knoten = knoten;
    }

    /**
     * Berechnet den Radius der Darstellung anhander der Groe�er der Zeichnflaeche und dem gesetzten Anteil des Radius an dieser Flaeche. Orientiert sich an der kleineren Dimension. Die uebergebenen Werte muessen groe�er 0 sein, sonst wird 1 zurueckgegeben.
     *
     * @param hoehe        Hoeher der Zeichenflaeche
     * @param breite       Breite der Zeichenflaeche
     * @param anteilRadius Anteil des Radius an der Groe�e der Zeichenflaeche.
     * @return Radius des Kreises.
     */
    private static int berechneRadius(int hoehe, int breite, int anteilRadius) {
        if (hoehe <= 0 || breite <= 0 || anteilRadius <= 0)
            return 1;

        return Math.min(hoehe, breite) / anteilRadius;
    }

    /**
     * Berechnet den Mittelpunkt der Knoten auf der x-Achse.
     *
     * @param kreisposition An welcher Position steht der Knoten.
     * @param breite        Breite der Zeichenflaeche
     * @param anzahlKreise  Anzahl der Knoten
     * @return Mittelpunkt des Knotens
     */
    private static int berechneMittlePunktKoordinaten(int kreisposition, int breite, int anzahlKreise) {
        int gleicheDistanz = breite / (anzahlKreise + 1);

        return gleicheDistanz * (kreisposition + 1);
    }

    /**
     * Zeichnet den Kreis auf der Graphic.
     *
     * @param g Graphic auf der gezeichnet werden soll.
     */
    public void zeichne(Graphics2D g) {
        Color tmp = g.getColor();

        g.setColor(Color.WHITE);
        g.fillOval(startPunkt.x, startPunkt.y, radius, radius);

        g.setColor(Color.BLACK);
        g.drawOval(startPunkt.x, startPunkt.y, radius, radius);
        g.drawString(knoten.toString(), mittelpunkt.x-2, mittelpunkt.y+5);

        g.setColor(tmp);
    }

    /**
     * Versetzt den Mittelpunkt des Kreises auf die uebergebenen Koordinaten und berechnet daraus die Startkoordinaten zum Zeichnen des Kreises.
     *
     * @param x x-Koordinate
     * @param y y-Koordinate
     */
    public void bewege(int x, int y) {
        mittelpunkt.x = x;
        mittelpunkt.y = y;

        berechneStartpunkt();
    }

    /**
     * Berechnet den Eckpunkt des umgebenden Rechtecks des Kreises anhand des Mittelpunktes.
     */
    private void berechneStartpunkt() {
        /*
         * Der Radius muss halbiert abgezugen werden, da von einem Kreis die
		 * Linke untere Ecke als Startkoordinate geweahlt wird. Diese muss
		 * entsprechend vom Mittelpunkt verschoben werden.
		 */
        startPunkt.x = mittelpunkt.x - radius / 2;
        startPunkt.y = mittelpunkt.y - radius / 2;
    }

    /**
     * Berechnet die Koordinaten des Mittelpunktes anhand der Reihenfolge des Knotens, der Zeichenflaeche und des Anteils des Radius an der Zeichenflaeche.
     *
     * @param position     Position des Knotens in der sortierten Liste der Knoten
     * @param hoehe        Hoehe der Zeichenflaeche.
     * @param breite       Breite der Zeichenflaeche.
     * @param anteilRadius Anteil des Radius an der Zeichenflaeche.
     * @param anzahlKreise Anzahl der Knoten, die dargestellt werden.
     */
    public void berechneKoordinaten(int position, int hoehe, int breite, int anteilRadius, int anzahlKreise) {

        this.radius = berechneRadius(hoehe, breite, anteilRadius);

        mittelpunkt.x = berechneMittlePunktKoordinaten(position, breite, anzahlKreise);

        mittelpunkt.y = hoehe / 2;

        berechneStartpunkt();
    }

    /**
     * Ermittelt, ob die uebergebenen Koordinaten innerhalb des Kreises liegen.
     *
     * @param x x-Koordinate
     * @param y y-Koorindate
     * @return Wahrheitswert
     */
    public boolean isInside(int x, int y) {
        Point p = new Point(x, y);

        return p.distance(mittelpunkt) < radius / 2;
    }

    /**
     * @return the mittelpunkt
     */
    public Point getMittelpunkt() {
        return mittelpunkt;
    }
}
