/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie k�nnen es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder sp�teren
 *     ver�ffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es n�tzlich sein wird, aber
 *     OHNE JEDE GEW�HRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gew�hrleistung der MARKTF�HIGKEIT oder EIGNUNG F�R EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License f�r weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem.elemente;

import uni.wue.boem.CommonSettings;
import uni.wue.boem.daten.Knoten;

import java.awt.*;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Die Klasse stellt die darstellung eines Kreisbogens dar zur Visualisierung einer Kante im Graphen und enthaelt alle Methoden zur Berechnung der Darstellung mithilfe der entsprechenden Knoten.
 *
 * @author Dennis van der Wals
 */
public class Arc {
    private final LinkedList<Knoten> knoten;
    private final Point startPunkt, mittelPunkt;
    private int breite, hoehe, startAngle, arcAngle;
    private boolean hervorheben;

    /**
     * Konstruktor. Die Knoten stellen Start- und Endpunkt des Bogens dar, ihre Reihenfolge ist egal. Sie duerfen nicht <code>null</code> sein.
     *
     * @param a Erster Knoten
     * @param b Zweiter Knoten
     */
    public Arc(Knoten a, Knoten b) {
        if (a == null || b == null)
            throw new IllegalArgumentException("Vertex a and b must not be NULL.");

        knoten = new LinkedList<>();
        knoten.add(a);
        knoten.add(b);

        startAngle = CommonSettings.DEGREE_0;
        arcAngle = CommonSettings.DEGREE_180;

        startPunkt = new Point();
        mittelPunkt = new Point();
    }

    /**
     * Zeichnet den berechneten Kreisbogen in der uebergebenen Farbe auf die Graphic. Setzt die Farbe anschlie�end zurueck.
     *
     * @param g           Graphic, auf die gezeichnet werden soll.
     * @param farbe       Farbe des Bogens.
     * @param zeichneOben legt fest, ob der Bogen in der obene Haelfte gezeichnet werden soll.
     */
    public void draw(Graphics2D g, Color farbe, boolean zeichneOben) {
        if (zeichneOben)
            startAngle = CommonSettings.DEGREE_0;
        else
            startAngle = CommonSettings.DEGREE_180;
        arcAngle = CommonSettings.DEGREE_180;

        Color tmp = g.getColor();
        Stroke tmpStroke = g.getStroke();

        if (hervorheben)
            g.setStroke(new BasicStroke(CommonSettings.HIGHLIGHTED_STROKE));
        else
            g.setStroke(new BasicStroke(CommonSettings.BASIC_STROKE));

        g.setColor(farbe);
        g.drawArc(startPunkt.x, startPunkt.y, breite, hoehe, startAngle, arcAngle);
        g.setStroke(tmpStroke);

        g.setColor(tmp);
    }

    /**
     * Berechnet die Parameter zum Zeichnen des spezifischen Kreisbogens.
     *
     * @param hoehe Die Hoehe der Darstellung zur Bestimmung der Kruemmung des
     *              Bogens
     * @param width Die Breite der Darstellung zur Bestimmung der Kruemmung des
     *              Bogens
     */
    public void berechneKoordinaten(int hoehe, int width) {
        if (hoehe <= 0) {
            System.err.println("Invalid height while computing of the circular arc. The height must be greater than 0.");
            return;
        }

		/*
         * Sortiere die Knoten, damit die Ausrichtung immer positiv ist. Wichtig
		 * fuer den Fall, dass die Knoten durchs verschieben die Reihenfolge
		 * tauschen
		 */
        Collections.sort(knoten);

        this.startPunkt.x = knoten.getFirst().getKreis().getMittelpunkt().x;

        this.breite = knoten.getLast().getKreis().getMittelpunkt().x - this.startPunkt.x;
        this.hoehe = (hoehe * breite) / width;

        this.startPunkt.y = knoten.getFirst().getKreis().getMittelpunkt().y - this.hoehe / 2;

        this.mittelPunkt.x = this.startPunkt.x + breite / 2;
        this.mittelPunkt.y = knoten.getFirst().getKreis().getMittelpunkt().y;
    }

    public String toString() {
        return String.format("(%d, %d): %d x %d, %d° -> %d°", startPunkt.x, startPunkt.y, breite, hoehe, startAngle, arcAngle);
    }

    /**
     * Die Koordinaten sind nahe der Bogens, wenn der Wert nahe 1 ist.
     *
     * @param mouseX x-Koordinate (des Mauszeigers z.B.)
     * @param mouseY y-Koordinate (des Mauszeigers z.B.)
     * @param oben   legt fest, ob der Bogen in der oberen Haelfte gezeichnet wurde.
     * @return Ergebnis der Ellipsengleichung unter Ber�cksichtigung der
     * Ausrichtung. 100 bedeutet, die Koordinaten liegen nicht auf der
     * selben Seite, wie der Bogen.
     */
    public double abstandZumRand(int mouseX, int mouseY, boolean oben) {

        double y = mittelPunkt.y - mouseY;
        boolean klickIstOben = y > 0;

        if (klickIstOben == oben) {

            double b = this.hoehe / 2;
            double a = this.breite / 2;

            double x = mouseX - mittelPunkt.x;

            return (((x * x) / (a * a)) + ((y * y) / (b * b)));
        }

        return 100;
    }

    /**
     * @param hervorheben the hervorheben to set
     */
    public void setHervorheben(boolean hervorheben) {
        this.hervorheben = hervorheben;
    }

}
