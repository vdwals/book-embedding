/*
 * Copyright (c) 2015. Dennis van der Wals
 *
 * This file is part of book-embedding.
 *
 *     book-embedding is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     book-embedding is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with book-embedding.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     Diese Datei ist Teil von book-embedding.
 *
 *     book-embedding ist Freie Software: Sie können es unter den Bedingungen
 *     der GNU General Public License, wie von der Free Software Foundation,
 *     Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
 *     veröffentlichten Version, weiterverbreiten und/oder modifizieren.
 *
 *     book-embedding wird in der Hoffnung, dass es nützlich sein wird, aber
 *     OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
 *     Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
 *     Siehe die GNU General Public License für weitere Details.
 *
 *     Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
 *     Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>.
 */

package uni.wue.boem;

import uni.wue.boem.daten.Kante;
import uni.wue.boem.daten.Knoten;
import uni.wue.boem.daten.MyColors;
import uni.wue.boem.daten.Seite;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * Klasse zum laden und speichern einer Bucheinbettung,
 *
 * @author Dennis van der Wals
 */
public class IO {
	private static final JFileChooser jf = new JFileChooser(new File(System.getProperty("user.dir"))) {
		/**
		 * Generated SerialID
		 */
		private static final long serialVersionUID = 1656228015807998563L;

		public void updateUI() {
			putClientProperty("FileChooser.useShellFolder", Boolean.FALSE);
			super.updateUI();
		}
	};
	private static Seite[] seiten;
	private static LinkedList<Knoten> knoten;

	/**
	 * Die Funktion speichert die uebergebenen Arrays im richtigem Format in
	 * eine ausgewaehlte Datei.
	 *
	 * @param k
	 *            LinkedList mit Knoten.
	 * @param s
	 *            Array mit Seiten.
	 * @return 0 = nicht gespeichert, 1 = gespeichert, -1 = fehler
	 */
	public static int speichern(LinkedList<Knoten> k, Seite[] s) {
		jf.putClientProperty("FileChooser.useShellFolder", Boolean.FALSE);
		knoten = k;
		seiten = s;

		int auswahl = jf.showSaveDialog(null);

		/*
		 * Wird nur aktiv, wenn der Benutzer die Auswahl einer Datei bestaetigt
		 * hat.
		 */
		if (auswahl == JFileChooser.CANCEL_OPTION)
			return 0;

		if (jf.getSelectedFile().isDirectory())
			System.err.println("A directory cannot be chosen!");

		else if (jf.getSelectedFile().exists())
			auswahl = JOptionPane.showConfirmDialog(null, "The chosen file already exists. Shall it be overwritten?",
					"Overwrite file", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

		else
			auswahl = JOptionPane.showConfirmDialog(null, "The chosen file doesn't exists yet. Shall it be created?",
					"Create file", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

		/*
		 * Wenn eine gueltige Datei ausgewaehlt und die notwendige Operation
		 * (erzeugen/ueberschreiben) bestaetigt wurde.
		 */
		if (auswahl == JOptionPane.YES_OPTION) {
			try {
				if (!jf.getSelectedFile().exists() && !jf.getSelectedFile().createNewFile()) {
					System.err.println("File could not be created.");
					return -1;
				}
			} catch (IOException e) {
				e.printStackTrace();
				return -1;
			}

			// Schreibt die Daten in die neue Datei,
			try {
				schreibe(jf.getSelectedFile());
				System.out.println("File was saved.");
				return 1;

			} catch (IOException e) {
				System.err.println("File could not be saved.");
				e.printStackTrace();
				return -1;
			}
		}

		return 0;
	}

	/**
	 * Oeffnet eine ausgewaehlte Datei und uebertraegt die Knoten, Kanten und
	 * Seiten in die statischen Arrays: knoten, seiten.
	 *
	 * @return Gibt zurueck, ob der Graph geladen werden konnte.
	 */
	public static boolean laden() {
		int auswahl = jf.showOpenDialog(null);

		if (auswahl != JFileChooser.CANCEL_OPTION && jf.getSelectedFile().isFile()) {
			try {
				lese(jf.getSelectedFile());
				System.out.println("File was loaded.");
				return true;
			} catch (FileNotFoundException e) {
				System.err.println("File could not be loaded.");
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

	/**
	 * Schreibt die Daten aus den Arrays knoten und seiten im bestimmten Format
	 * in eine Datei.
	 *
	 * @param f
	 *            Zieldatei
	 * @throws IOException
	 *             Wirft ausnahmen bei Dateizugriffsfehlern.
	 */
	private static void schreibe(File f) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter(f));

		bw.write("# Number of Nodes (N)");
		bw.newLine();
		bw.write(knoten.size() + "");
		bw.newLine();

		bw.write("# Number of pages (K)");
		bw.newLine();
		bw.write(seiten.length + "");
		bw.newLine();

		bw.write("# Permutation of nodes");
		bw.newLine();
		for (Knoten k : knoten) {
			bw.write(k.toString());
			bw.newLine();
		}

		bw.write("# Edges with starting node, ending node and assigned page in rectangular brackets");
		bw.newLine();
		for (Seite aSeiten : seiten) {
			for (int j = 0; j < aSeiten.getKanten().size(); j++) {
				bw.write(aSeiten.getKanten().get(j).toString());
				bw.newLine();
			}
		}

		bw.close();
	}

	/**
	 * Liest Knoten, deren Reihenfolge, Seiten und Kanten aus einer Datei aus.
	 *
	 * @param f
	 *            Datei mit Knoten, Permutation, Seiten und Kanten
	 * @throws FileNotFoundException
	 *             Wirft Exception, wenn Datei nicht gefunden wurde.
	 */
	private static void lese(File f) throws FileNotFoundException {
		Scanner dateiScanner = new Scanner(f);
		String zeile;

		knoten = null;
		seiten = null;

		// Finde Anzahl der Knoten
		int anzahlKnoten = 0;
		while (dateiScanner.hasNextLine()) {
			zeile = entferneKommentar(dateiScanner.nextLine());

			if (zeile.matches("\\d+")) {
				knoten = new LinkedList<>();
				anzahlKnoten = Integer.parseInt(zeile);
				break;
			}
		}

		if (anzahlKnoten == 0) {
			dateiScanner.close();
			throw new IllegalArgumentException("Number of vertices could not be found.");
		}

		// Finde Anzahl der Seiten
		while (dateiScanner.hasNextLine()) {
			zeile = entferneKommentar(dateiScanner.nextLine());

			if (zeile.matches("\\d+")) {
				seiten = new Seite[Integer.parseInt(zeile)];

				for (int i = 0; i < seiten.length; i++) {
					Color c = MyColors.get(i);

					seiten[i] = new Seite(i, c);
				}

				break;
			}
		}

		if (seiten == null) {
			dateiScanner.close();
			throw new IllegalArgumentException("Number of pages could not be found.");
		}

		// Lese die Reihenfolge der Knoten
		while (knoten.size() < anzahlKnoten) {
			zeile = entferneKommentar(dateiScanner.nextLine());

			if (zeile.matches("\\d+")) {
				Knoten neuerKnoten = new Knoten(Integer.parseInt(zeile));

				if (knoten.contains(neuerKnoten)) {
					dateiScanner.close();
					throw new IllegalArgumentException(
							"The vertex " + neuerKnoten + " exists twice in the permutation.");
				}

				knoten.add(neuerKnoten);
			}

			if (!dateiScanner.hasNextLine()) {
				dateiScanner.close();
				throw new IllegalArgumentException("The permutation of the vertices is incomplete.\n Only "
						+ knoten.size() + " vertices were found in the permutation.");
			}
		}

		// Lese Kanten ein
		int kanten = 0;
		while (dateiScanner.hasNextLine()) {
			zeile = entferneKommentar(dateiScanner.nextLine());

			if (zeile.matches("\\d+ \\d+ \\[\\d+\\]")) {
				/*
				 * Zerteile die Zeile um die einzelnen Zahlen interpretieren zu
				 * k�nnen.
				 */
				String[] pars = zeile.split(" ");
				pars[2] = pars[2].replace("[", "").replace("]", "");

				int a = Integer.parseInt(pars[0]);
				int b = Integer.parseInt(pars[1]);
				int page = Integer.parseInt(pars[2]);

				Knoten ka = null;
				Knoten kb = null;

				/*
				 * Suche die Knoten anhand der hinterlegten Nummer. Dies ist
				 * wichtig, da die Permutation nicht immer mit der Nummerierung
				 * �bereinstimmen muss.
				 */
				for (int i = 0; i < knoten.size() && (ka == null || kb == null); i++) {
					Knoten vergleich = knoten.get(i);
					if (vergleich.getNummer() == a) {
						ka = vergleich;

					} else if (vergleich.getNummer() == b) {
						kb = vergleich;
					}
				}

				/*
				 * Erzeugt eine neue Kante, die sp�ter �ber die
				 * entsprechende Seite oder Knoten abgerufen werden kann. Es ist
				 * nich notwendig die Kanten in einer zus�tzlichen
				 * Datenstruktur abzulegen.
				 */
				new Kante(ka, kb, seiten[page]);
				kanten++;
			}
		}

		dateiScanner.close();

		if (kanten == 0)
			throw new IllegalArgumentException("No edges were found.");
	}

	/**
	 * Entfernt alle Symbole aus einem String die nach einem Kommentarsymbol #
	 * stehen, inklusive des Symbols.
	 *
	 * @param eingabe
	 *            String, der von Kommentaren befreit werden soll
	 * @return String ohne Kommentar
	 */
	private static String entferneKommentar(String eingabe) {
		int kommentarStart = eingabe.indexOf("#");

		if (kommentarStart != -1)
			return eingabe.substring(0, kommentarStart).trim();

		return eingabe;
	}

	/**
	 * @return the seiten
	 */
	public static Seite[] getSeiten() {
		return seiten;
	}

	/**
	 * @return the knoten
	 */
	public static LinkedList<Knoten> getKnoten() {
		return knoten;
	}
}
